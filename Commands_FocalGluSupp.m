%% Simulation code to support description of observations of Focal Glutamate
% Requirements 
% OGD cannot be too long as the neuron should recover, so we increase pumpScale from 1.0 to 3.0
% We add diffusion to an external bath to obtain differences in maximal glutamate, parameter BE.

%% Test Commands; used during development. 
% aa= [5 28 30 1 0.001]; %onset, offset, strength, duration/period of stimulation cycle, duty cycle (percentage on)
% % Just stimulation every minute to evoke one spike.
% % Total simulated time tfinal=100 minutes also yields steady state if it should be adapted
% [t,y,M]=Main('juststim', BE=1e-3, alphae0=0.2, excite =aa, tfinal=30, beta1=10000, beta2=10000);
% % Testing differences due to small or large ECS
% [t,y,M]=Main('juststim', BE=1e-3, alphae0=0.98, excite =aa, tfinal=30, beta1=10000, beta2=10000);

% % Now we add OGD for four minutes with simulated time tfinal=30 minutes.
% aa= [2.5 25.5 0 1 0.01]; %use a small stimulus to test 
% [t1,y1,M1]=Main('OGD_test_recover', BE=1e-3,alphae0=0.98, tstart=10, tend=14,perc=0.3, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=2);
%
% We increase the duty cycle to evoke more spikes to have more Ca entry into the cell.
% Stimulus amplitude has to be increased to overcome stronger NKA (which controls excitability)
% aa= [2.5 25.5 60 1 0.01];  %stimulus every minute 
% aa= [2.5 25.5 60 2 0.005]; %stimulus every two minutes

% Simulating Four minutes of OGD with different exchange rates with the bath
% [t1,y1,M1]=Main('OGDstimrecover_weak'  , BE=1e-5,alphae0=0.2, excite =aa, tstart=10, tend=14,perc=0.3, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=3);
% [t2,y2,M2]=Main('OGDstimrecover_medium', BE=1e-4,alphae0=0.2, excite =aa, tstart=10, tend=14,perc=0.3, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=3);
% [t3,y3,M3]=Main('OGDstimrecover_strong', BE=1e-3,alphae0=0.2, excite =aa, tstart=10, tend=14,perc=0.3, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=3);
% close all;

%% Final simulations.
% Simulating four minutes (10 to 14) of OGD with different exchange rates
% with the bath, 5-fold changes, including stimulation 
aa= [0.5 29.5 60 2 0.0005]; %onset, offset, strength, duration/period of stimulation cycle, duty cycle (percentage on)
% We increase the duty cycle to evoke more spikes to have more Ca entry into the cell.
% Stimulus amplitude has to be increased to overcome stronger NKA (which controls excitability)

% Simulations with a large ECS alphae0=[0.8,0.98] deviate too much from data.
% Simulations with a small ECS alphae0=0.2
clc;
[t1,y1,M1]=Main('OGDstimrecover_weak'  , BE=2e-4,alphae0=0.2, excite =aa, tstart=10, tend=14,perc=0.1, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=3);
[t2,y2,M2]=Main('OGDstimrecover_medium', BE=1e-3,alphae0=0.2, excite =aa, tstart=10, tend=14,perc=0.1, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=3);
[t3,y3,M3]=Main('OGDstimrecover_strong', BE=5e-3,alphae0=0.2, excite =aa, tstart=10, tend=14,perc=0.1, tfinal=30, beta1=10, beta2=2, pumpScaleNeuron=3);
% close all;
%
mycolors = [0 .8 0; 0 .6 0 ; 0 .3 0];
figure(1);clf(1);plot(t1,y1(:,27),t2,y2(:,27),t3,y3(:,27));xlabel('time[min]');ylabel('[O_{2}]','linewidth',1);
colororder(mycolors)
legend('B=2e-4 ms^{-1}','B=1e-3 ms^{-1}','B=5e-4 ms^{-1}', 'Location','southeast')
saveas(figure(1),'images/O2level.png')
saveas(figure(1),'images/O2level.fig')
figure(2);clf(2);plot(t1,y1(:,26)./M1('VolPreSyn'),t2,y2(:,26)./M2('VolPreSyn'),t3,y3(:,26)./M3('VolPreSyn'),'linewidth',1);
xlabel('time[min]');ylabel('[Glu (mM)]');
colororder(mycolors);legend('B=2e-4 ms^{-1}','B=1e-3 ms^{-1}','B=5e-4 ms^{-1}');
saveas(figure(2),'images/Glulevel.png')
saveas(figure(2),'images/Glulevel.fig')
figure(3);clf(3);plot(t1,y1(:,7)/M1('VolPreSyn'),t2,y2(:,7)/M2('VolPreSyn'),t3,y3(:,7)/M3('VolPreSyn'),'linewidth',1);
xlabel('time[min]');ylabel('[Ca (mM)]');
colororder(mycolors);legend('B=2e-4 ms^{-1}','B=1e-3 ms^{-1}','B=5e-4 ms^{-1}');
saveas(figure(3),'images/Calevel.png')
saveas(figure(3),'images/Calevel.fig')

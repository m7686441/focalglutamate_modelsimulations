function [t,y,M] = Main(filename, varargin)
tic %To record time 
close all
[M,block] = Setting_variables(varargin);     % using all standard parameters, but adapt the parameters that are defined in the input argument.
M = parameters(M);                   % Calculate all other parameters.

[t0,y0]=initvals(M);

options = odeset('RelTol',1e-10, 'AbsTol', 1e-10, 'InitialStep',1e-12', 'MaxStep',0.0001); %Tolerances have to be strict for correct results.
[t,y] = ode15s(@(t,y) Model(t,y,M,block),[t0,M('tfinal')],y0,options);

mkdir(['images/' filename]); save([pwd '/images/' filename '/Workspace']); %save the dictionary M, the block, and the solution t,y.
plots(t,y,M,block,filename)

toc


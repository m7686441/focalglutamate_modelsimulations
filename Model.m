function [ODEs]= Model(t,y,M,block)
% t
if size(y,2) > 1
        NNa = y(:,1);
        NK = y(:,2);
        NCl = y(:,3);
        m = y(:,4);
        h = y(:,5);
        n = y(:,6);
        NCai = y(:,7);
        NN = y(:,8);
        NR = y(:,9);
        NR1 = y(:,10);
        NR2 = y(:,11);
        NR3= y(:,12);
        NI = y(:,13);
        ND = y(:,14);
        NNag = y(:,15);
        NKg = y(:,16);
        NClg = y(:,17);
        NCag = y(:,18);
        NGlug = y(:,19);
        Wi = y(:,20);
        Wg = y(:,21);
        NNae = y(:,22);
        NKe = y(:,23);
        NCle = y(:,24);
        NCac = y(:,25);
        NGluc = y(:,26);
        O2 = y(:,27);
else
        NNa = y(1);
        NK = y(2);
        NCl = y(3);
        m = y(4);
        h = y(5);
        n = y(6);
        NCai = y(7);
        NN = y(8);
        NR = y(9);
        NR1 = y(10);
        NR2 = y(11);
        NR3 = y(12);
        NI = y(13);
        ND = y(14);
        NNag = y(15);
        NKg = y(16);
        NClg = y(17);
        NCag = y(18);
        NGlug = y(19);
        Wi = y(20);
        Wg = y(21);
        NNae = y(22);
        NKe = y(23);
        NCle = y(24);
        NCac = y(25);
        NGluc = y(26);
        O2 = y(27);
end
if M('nosynapse')
    synapse_block = 0;
else
    synapse_block = 1;
end
        %--------------------------------------------------------------
        %---------Ionic amount and concentrations----------------------
        %--------------------------------------------------------------
%HGE: We comment some calculations below as these variables are now
%dynamic, and the quantities no longer conserved.
    % ECS
    We = M('Wtot') - Wi - Wg;
    % NNae = M('CNa') - NNag - NNa;
    % NKe = M('CK') - NKg - NK;
    % NCle = M('CCl') - NClg - NCl;
    NaCe = NNae./We;
    KCe = NKe./We;
    ClCe = NCle./We;

    % Neuron
    NGlui = NI;
    NaCi = NNa./Wi;
    KCi = NK./Wi;
    ClCi = NCl./Wi;
    CaCi = NCai./M('VolPreSyn');
    GluCi = NGlui./M('VolPreSyn');

    % Astrocyte
    NaCg = NNag./Wg;
    KCg = NKg./Wg;
    ClCg = NClg./Wg;
    GluCg = NGlug./M('VolPAP');
    CaCg = NCag./M('VolPAP');

    % Cleft
    % NCac = M('CCa') - NCai - NCag;
    % NGluc = M('CGlu') - NGlui - NGlug - ND - NN - NR - NR1- NR2 - NR3;
    CaCc = NCac./M('Volc');
    GluCc = NGluc./M('Volc');
    if any(GluCc <0)
        disp(strcat('GluCc<0, i.e.', num2str(GluCc)))
        % GluCc
    end
    
    % Voltages
    % if M('excite') %|| M('excite2')
        %V = Vtemp
        V = M('F')./M('C').*(NNa+NK+synapse_block.*2.*NCai-synapse_block.*(NGlui+NN+NR+NR1+NR2+NR3+ND)-NCl-M('NAi'));
    % else
    %     V = M('F')./M('C').*(NNa+NK+synapse_block.*2.*NCai-synapse_block.*(NGlui+NN+NR+NR1+NR2+NR3+ND)-NCl-M('NAi'));
    % end
    Vi = V ;% Needed for plotting
    Vg = M('F')./M('Cg').*(NNag + NKg + M('NBg') - M('NAg') - NClg + ...
                   synapse_block.*2.*NCag - synapse_block.*NGlug);
    
    % ==========================================================================
    % --------------------NEURON-------------------------------------------------
    % ===========================================================================
   
    % HH constants
    alpham = 0.32.*(V+52)./(1-exp(-(V+52)./4));
    betam = 0.28.*(V+25)./(exp((V+25)./5)-1);
    alphah = 0.128.*exp(-(V+53)./18);
    betah = 4./(1+exp(-(V+30)./5));
    alphan = 0.016.*(V+35)./(1-exp(-(V+35)./5));
    betan = 0.25.*exp(-(V+50)./40);

    if M('nogates')
        gates_block = 0;
        m = alpham./(alpham + betam);
        h = alphah./(alphah + betah);
        n = alphan./(alphan + betan);
    else
        gates_block = 1  ;
    end

    % Gated currents
    INaG = M('PNaG').*(m.^3).*(h).*(M('F').^2).*(V)./( ...
       M('R').*M('T')).*((NaCi - ...
                  NaCe.*exp(-(M('F').*V)./(M('R').*M('T'))))./( ...
                     1-exp(-(M('F').*V)./(M('R').*M('T')))));
    IKG = (M('PKG').*(n.^2)).*(M('F').^2).*(V)./( ...
       M('R').*M('T')).*((KCi - ...
                  KCe.*exp(-(M('F').*V)./(M('R').*M('T'))))./( ...
                     1-exp(-M('F').*V./(M('R').*M('T')))));
    IClG = M('PClG').*1./(1+exp(-(V+10)./10)).*( ...
       M('F').^2).*V./(M('R').*M('T')).*((ClCi - ...
                             ClCe.*exp(M('F').*V./(M('R').*M('T'))))./( ...
                                1-exp(M('F').*V./(M('R').*M('T')))));
    ICaG = M('PCaG').*m.^2.*h.*4.*M('F').^2./( ...
       M('R').*M('T')).*V.*((CaCi- ...
                    CaCc.*exp(-2.*(M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp(-2.*(M('F').*V)./(M('R').*M('T')))));

    % Leak currents
    INaLi = M('PNaLi').*(M('F').^2)./( ...
       M('R').*M('T')).*V.*((NaCi - ...
                    NaCe.*exp((-M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((-M('F').*V)./(M('R').*M('T')))));
    IKLi = M('PKLi').*M('F').^2./(M('R').*M('T')).*V.*((...
       KCi - ...
       KCe.*exp((-M('F').*V)./(M('R').*M('T'))))./( ...
          1-exp((-M('F').*V)./(M('R').*M('T')))));
    IClLi = M('PClLi').*(M('F').^2)./( ...
       M('R').*M('T')).*V.*((ClCi - ...
                    ClCe.*exp((M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((M('F').*V)./(M('R').*M('T')))));
    ICaLi = 4.*M('PCaLi').*(M('F').^2)./( ...
       M('R').*M('T')).*V.*((CaCi - ...
                    CaCc.*exp((-2.*M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((-2.*M('F').*V)./(M('R').*M('T')))));
    IGluLi = M('PGluLi').*M('F').^2./( ...
       M('R').*M('T')).*V.*((GluCi - ...
                    GluCc.*exp((M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((M('F').*V)./(M('R').*M('T')))));

    % Blockade
    % blockerExp = 1./(1+exp(M('beta1').*(t-M('tstart')))) + 1./( ...
    %    1+exp(-M('beta2').*(t-M('tend'))));
    % blockerExpAlt = 1./(1+exp(M('beta1').*(t-M('tstart')))) + M('perc')./( ...
    %    1+exp(-M('beta2').*(t-M('tend'))));
    % blockerExp = M('perc') + (1-M('perc')).*blockerExp;
    % blockerExp = blockerExpAlt
    %13-2-2024 Adapting to Oxygen as in Wei 2014 instead of manually blocking the pump
    blockerExp=1;
    O2bath = M('O2bath')*ones(size(t));
    O2bath(t>M('tstart') & t<M('tend'))=M('perc')*M('O2bath');


    % Na-K pump
    rho = 1./(1+exp((20-O2)/3));
    sigmapump = 1./7.*(exp(NaCe./67.3)-1);
    fpump = 1./(1+0.1245.*exp(-0.1.*M('F')./M('R')./M('T').*V) + ...
               0.0365.*sigmapump.*exp(-M('F')./M('R')./M('T').*V));
    Ipumpi = rho.*blockerExp.*M('pumpScaleNeuron').*fpump.*M('PNKAi').*( ...
                NaCi.^(1.5)./(NaCi.^(1.5)+M('nka_na').^1.5)).*(KCe./(KCe+M('nka_k')));

    % KCl cotransport
    JKCl = M('UKCl').*M('R').*M('T')./M('F').*(log(KCi)+log(ClCi)-log(KCe)-log(ClCe));

    % NCX
    INCXi = M('PNCXi').*(NaCe.^3)./(M('alphaNaNCX').^3+NaCe.^3).*(...
       CaCc./(M('alphaCaNCX')+CaCc)).*( ...
          NaCi.^3./NaCe.^3.*exp(M('eNCX').*M('F').*V./M('R')./M('T')) - ...
          CaCi./CaCc.*exp((M('eNCX')-1).*M('F').*V./M('R')./M('T')))./( ...
             1+M('ksatNCX').*exp((M('eNCX')-1).*M('F').*V./M('R')./M('T')));

    % EAAT
    beta_EAAT=0.0292; %mV^-1 from Flanagan PLOS CB 2018
    V_EAATi = M('R')*M('T')/M('F')/2*log(NaCe.^3./NaCi.^3.*KCi./KCe .* ...
                                          M('HeOHa').*GluCc./GluCi);
    % JEAATi = M('PEAATi')*exp(-beta_EAAT*(Vi-V_EAATi));
    JEAATi = M('PEAATi')*(exp(-beta_EAAT*(Vi-V_EAATi))-1); % Version Breslin 2018
    % JEAATi =  M('PEAATi').*M('R').*M('T')./M('F').*log((NaCe.^3)./(NaCi.^3) .* ...
    %                                 KCi./KCe.*M('HeOHai').*GluCc./GluCi);

    % =========================================================================
    % ---------------------------------------------------------------------
    % ========================================================================

    k1 = M('k1max').*CaCi./(CaCi+M('KM'));
    gCa = CaCi./(CaCi+M('KDV'));
    k2 = M('k20')+gCa.*M('k2cat');
    kmin2cat = M('k2cat').*M('kmin20')./M('k20');
    kmin2 = M('kmin20')+gCa.*kmin2cat;
    docking = 1./(1+exp(100*(ND-2.5))); %Saturation of docking glutamate. 
    % ===========================================================================
    % --------------------------ASTROCYTE-----------------------------------------
    % ============================================================================

    % Na-K pump
    sigmapumpA = 1./7.*(exp(NaCe./67.3)-1);
    fpumpA = 1./(1+0.1245.*exp(-0.1.*M('F')./M('R')./M('T').*Vg) + ...
                0.0365.*sigmapumpA.*exp(-M('F')./M('R')./M('T').*Vg));
    Ipumpg = rho*M('pumpScaleAst').*blockerExp.*fpumpA.*M('PNKAg').*( ...
          NaCg.^(1.5)./(NaCg.^(1.5)+M('nka_na_g').^1.5)).*(KCe./(KCe+M('nka_k_g')));

    % Leak
    IKLg = M('PKLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((KCg - ...
                     KCe.*exp((-M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((-M('F').*Vg)./(M('R').*M('T')))));
    IClLg = M('PClLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((ClCg - ...
                     ClCe.*exp((M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((M('F').*Vg)./(M('R').*M('T')))));
    INaLg = M('PNaLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((NaCg - ...
                     NaCe.*exp((-M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((-M('F').*Vg)./(M('R').*M('T')))));
    IGluLg = M('PGluLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((GluCg - ...
                     GluCc.*exp((M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((M('F').*Vg)./(M('R').*M('T')))));
    ICaLg = 4.*M('PCaLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((CaCg - ...
                     CaCc.*exp((-2.*M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((-2.*M('F').*Vg)./(M('R').*M('T')))));

    % NKCC1
    JNKCC1 = M('PNKCC1').*M('R').*M('T')./M('F').*(log(KCe) + log(NaCe) ...
                                   + 2.*log(ClCe) - log(KCg) ...
                                   - log(NaCg) - 2.*log(ClCg));
    % Kir4.1
    Vkg = M('R').*M('T')./M('F').*log(KCe./KCg);
    minfty = 1./(2+exp(1.62.*(M('F')./M('R')./M('T')).*(Vg-Vkg)));
    IKir = M('PKir').*minfty.*KCe./(KCe+M('KCe_thres')).*(Vg-Vkg);
    % IKir = M('GKir.*(Vg-Vkg).*sqrt(KCe)./(1+exp((Vg - Vkg - 34)./19.23)));

    % GLT-1
    beta_EAAT=0.0292; %mV^-1 from Flanagan PLOS CB 2018
    V_EAATg = M('R')*M('T')/M('F')/2*log(NaCe.^3./NaCg.^3.*KCg./KCe .* ...
                                          M('HeOHa').*GluCc./GluCg);
    % JEAATg = M('PEAATg')*exp(-beta_EAAT*(Vg-V_EAATg));
    JEAATg = M('PEAATg')*(exp(-beta_EAAT*(Vg-V_EAATg))-1); %Version Breslin 2018
%OLD; reversal potential as current...
    % JEAATg = M('PEAATg').*M('R').*M('T')./M('F').*log(NaCe.^3./NaCg.^3.*KCg./KCe .* ...
    %                                       M('HeOHa').*GluCc./GluCg);

    % NCX
    INCXg = M('PNCXg').*(NaCe.^3)./(M('alphaNaNCX').^3 + ...
                               NaCe.^3).*(CaCc./(M('alphaCaNCX')+CaCc)).*( ...
                                  NaCg.^3./NaCe.^3.*exp(M('eNCX').*M('F').*Vg./M('R')./M('T')) - ...
                                  CaCg./CaCc.*exp((M('eNCX')-1).*M('F').*Vg./M('R')./M('T')))./(...
                                     1+M('ksatNCX').*exp( ...
                                        (M('eNCX')-1).*M('F').*Vg./M('R')./M('T')));
    
    % =========================================================================
    % ----------------------------VOLUME DYNAMICS------------------------------
    % =========================================================================
    SCi = NaCi+KCi+ClCi+M('NAi')./Wi ;
    SCe = NaCe+KCe+ClCe+M('NAe')./We + M('NBe')./We;
    SCg = NaCg + KCg + ClCg + M('NAg')./Wg + M('NBg')./Wg;
    delpii = M('R').*M('T').*(SCi-SCe);
    fluxi = M('LH20i').*(delpii);
    delpig = M('R').*M('T').*(SCg-SCe);
    fluxg = M('LH20g').*(delpig);

    Voli = Wi./M('Wi0').*100;
    Volg = Wg./M('Wg0').*100;

    % =========================================================================
    % ----------------------------POSTSYNAPTIC RESPONSE------------------------
    % =========================================================================

   % IAMPA = M('gAMPA.*mAMPA.*(Vpost-M('VAMPA)

    % ==========================================================================
    % ----------------------------INTERVENTIONS---------------------------------
    % ==========================================================================

    if M('block') ==1 
        for i = 1:size(block,2) %Each row has a seperate label that needs to be blocked.
            key = block{i}(1);
            val_start = str2double(block{i}(2));
            val_end = str2double(block{i}(3));
            blockOther = (1./(1+exp(100.*(t-val_start))) + ...
                          1./(1+exp(-100.*(t-val_end))));
            %fprintf('blocking %s current from time %4.2f untill time %4.2f \n', key,val_start,val_end)
            if key == 'INaG'
                INaG = INaG.*blockOther;
            elseif key == 'IKG'
                IKG = IKG.*blockOther;
            elseif key == 'IClG'
                IClG = IClG.*blockOther;
            elseif key == 'JKCl'
                JKCl = JKCl.*blockOther;
            elseif key == 'ICaG'
                ICaG = ICaG.*blockOther;
            elseif key == 'INCXi'
                INCXi = INCXi.*blockOther;
            elseif key == 'JEAATi'
                JEAATi = JEAATi.*blockOther;
            elseif key == 'IKir'
                IKir = IKir.*blockOther;
            elseif key == 'JNKCC1'
                JNKCC1 = JNKCC1.*blockOther;
            elseif key == 'JEAATg'
                JEAATg = JEAATg.*blockOther;
            elseif key == 'INCXg'
                INCXg = INCXg.*blockOther;
            elseif key == 'WaterN'
                fluxi = fluxi.*blockOther;
            elseif key == 'WaterA'
                fluxg = fluxg.*blockOther;
            elseif key == 'IClLg'
                partialLeakblock = val_(2) + (1-val_(2)).*blockOther;
                IClLg = IClLg.*partialLeakblock;
            end
        end
    elseif M('block') == 0  %This means that block=0 and so nothing happens
    else                            %If block is not 0 and not a string of length 3 (block = ["BLOCK1", starttime, endtime]), something is wrong.
        disp('block is not well defined. Make sure to define it as [["BLOCK1", starttime, endtime]; ["BLOCK2", starttime, endtime], ...]')
    end
    if M('excite')== 1
%HGE 3-5-24: Added tt to have a phase difference, i.e. stimulation at odd minutes, and OGD onset + offset at even minutes 
        tt=t+1;
        blocker_Excite = 1 - (1./(1+exp(1000.*(tt-M('excite_1')))) + ...
                              1./(1+exp(-1000.*(tt-M('excite_2')))));
        %IExcite = blocker_Excite.*arg_excite(2)./2./M('F').*(1-square(array(5.*t),duty=arg_excite(3)));
        dur_ = M('excite_4');
        duty_ = M('excite_5');
%HGE 17-1-24:Changed to make the pulse shorter, a little bit stronger, just a few spikes, and simpler notation.
        IExcite = M('excite_3')/M('F')*blocker_Excite.*(1+square(2*pi*tt/dur_,100*duty_))/2;
        % IExcite = blocker_Excite.*M('excite_3')./2./M('F').*(1-square(2.*pi.*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));%Original
    elseif M('excite') == 0
        IExcite = 0    ;
        %IExcite = blocker_Excite.*4.5./M('F')
    end
    if M('excite2')== 1
        blocker_Excite2 = 1 - (1./(1+exp(100.*(t-M('excite2_1')))) + ...
                              1./(1+exp(-100.*(t-M('excite2_2')))));
        %IExcite2 = blocker_Excite2.*arg_excite2(2)./2./M('F').*(1-signal.square(array(5.*t),duty=arg_excite2(3)));
        dur_ = M('excite2_4');
        duty_ = M('excite2_5');
        IExcite2 = blocker_Excite2.*M('excite2_3')./2./M('F').*(1-signal.square(2.*pi.*array(t+2.1).*(1-duty_)./(dur_./60),duty=100*duty_));
    elseif M('excite2')==0
        IExcite2=0;
    end


    if M('astblock')==1
        startastblock = M('astblock_1');
        endastblock = M('astblock_2');
        astblock = (1./(1+exp(500.*(t-startastblock))) + ...
            1./(1+exp(-500.*(t-endastblock))));
    elseif M('astblock') ==0
        astblock = 1;
    else
        error('Astblock is not a vector of [TSART, TEND] and not 0. Make sure that you assign the right value to Astblock')
    end

    % ==========================================================================
    % ----------------------------FINAL MODEL-----------------------------------
    % ==========================================================================

    if any(GluCg <0) || any(GluCc < 0) || any(GluCi <0)
        disp('glutamate is negative...')
    end

    ODE1 = [ ... % Neuron
       ((-1./M('F').*(INaG+INaLi+3.*Ipumpi))-synapse_block.*3./M('F').*INCXi + synapse_block.*3.*JEAATi ), ...%Na
       (-1./M('F').*(IKG+IKLi-2.*Ipumpi)-synapse_block.*JEAATi - JKCl), ...%K
       (1./M('F').*(IClG+IClLi)) - JKCl, ... %Cl
       gates_block.*(alpham.*(1-m)-betam.*m), ...
       gates_block.*(alphah.*(1-h)-betah.*h), ...
       gates_block.*(alphan.*(1-n)-betan.*n), ...
       synapse_block.*(-1./2./M('F')).*(ICaG+ICaLi -INCXi), ...%Ca
       ... % GLUTAMATE RECYCLING
       (k1.*ND-(M('kmin1')+k2).*NN+kmin2.*NR), ...
       (k2.*NN-(kmin2+3.*M('k3').*CaCi).*NR + M('kmin3').*NR1), ...
       (3.*M('k3').*CaCi.*NR-(M('kmin3')+2.*M('k3').*CaCi).*NR1+2.*M('kmin3').*NR2), ...
       (2.*M('k3').*CaCi.*NR1-(2.*M('kmin3')+M('k3').*CaCi).*NR2+3.*M('kmin3').*NR3), ...
       (M('k3').*CaCi.*NR2-(3.*M('kmin3')+M('k4')).*NR3), ...
       synapse_block.*(-docking.*NI/M('trec') + JEAATi + 1./M('F').*IGluLi), ...
       (docking.*NI/M('trec')-k1.*ND+M('kmin1').*NN), ...
       ... % ASTROCYTE
       ((-1./M('F')).*(3.*Ipumpg + INaLg + synapse_block.*3.*INCXg )+JNKCC1+synapse_block.*3.*JEAATg), ...%Na
       ((-1./M('F')).*(-IKir - 2.*Ipumpg + IKLg)+ JNKCC1-synapse_block.*JEAATg), ...%K
       astblock.*(2.*JNKCC1 + 1./M('F').*IClLg) , ... %Cl
       ... % ASTROCYTE Calcium+Glu
       synapse_block.*astblock.*(-1./2./M('F')).*(-INCXg + ICaLg), ...%Ca
       synapse_block.*astblock.*(JEAATg + 1./M('F').*IGluLg), ... %Glu
       ... % WATER
       fluxi, ...
       astblock.*fluxg
       ];
% We have tested adding a puff of Glutamate as in the experiment, works as in the data, not shown as we cannot calibrate.
%    if (t>=25 & t<=25.1)
%        M('NGlu_B')=.0001;
%    end

% Adding dynamic concentrations in the cleft and Oxygen
    ODEs=[ODE1,...
        -ODE1(1)-ODE1(15) + M('BE')*(M('NNa_B')-NNae),...
        -ODE1(2)-ODE1(16) + M('BE')*(M('NK_B' )-NKe),...
        -ODE1(3)-ODE1(17) + M('BE')*(M('NCl_B')-NCle),...
        -ODE1(7)-ODE1(18) + M('BE')*(M('NCa_B')-NCac),...
        -ODE1(13)-ODE1(19)+ M('BE')*(M('NGlu_B')-NGluc),...
        -M('O2alpha')/M('F').*(Ipumpi+Ipumpg) + M('O2eps0').*(O2bath-O2)];

    if M('excite')== 1
        %ODEs(19) =  M('F')./M('C').*(ODEs(0)+ODEs(1)+synapse_block.*2.*(ODEs(6))-synapse_block.*(ODEs(7)+ODEs(8)+ODEs(9)+ODEs(10)+ODEs(11)+ODEs(12)+ODEs(13))-ODEs(2) + IExcite+IExcite2)
        ODEs(:,1) = ODEs(:,1) + IExcite + IExcite2; % Inject current into the neuron in the form of a bit of sodium injection
    end
    ODEs = ODEs.*60.*1e3;
    ODEs=ODEs';
    ODEs=real(ODEs);
    

%   FIM: From Python Code
%     if args
%         if 'ax1' in args(0) or 'ax2' in args(0):
%             temp_ = args(0);
%             return eval(temp_(3:))     
%         else
%             return eval(args(0))       
%         end
% 
%     else
%         return EEs     
%     end

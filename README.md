# FocalGlutamate_ModelSimulations

## Purpose
This code accompanies a manuscript by Ziebarth, Reiner and others on Focal Glutamate and provides Model Simulations to support our descriptions.

## Credits for this code
Manu Kalia developed this model during his PhD in Python.  
Fleur van Alphen translated this to Matlab, improved documentation and identified points for improvement during her MSc Thesis.  
Hil Meijer supervised Manu Kalia and Fleur van Alphen, and adapted these codes to this final version.  
Andreas Reiner and Tim Ziebarth gave extensive input on earlier simulations.  

## Reproducing the simulations
We use a dictionary to keep code transparent, which requires Matlab R2022b or later. This code has been tested with Matlab 2023b. After you have downloaded these m-files, execute the main script Commands_FocalGlu.m in Matlab.  
Other files:  
-Setting_variables.m script to initialize parameter choices  
-parameters.m        script to initialize parameter choices  
-initvals.m          script to initialize the initial state after all choices  
-Model.m             script containing the ODEs  
-plots.m             script creating the various plots for Na+,K+,Cl-,Ca2+,Glu and volume for neuron, astrocyte and synaptic cleft.  
-main.m              script calling all of the above subscripts in the correct order.  
Finally, within Commands_FocalGlu.m, we also plot the glutamate and calcium time traces for the three values of diffusion strength.

Upon running, three folders are created each containing the plots as well as the complete workspace after the simulation. The top subfolder contains the combined glutamate and calcium plots.

## License
CC BY-NC 4.0; Feel free to use and adapt for academic research and education.

## Project status
This work is part of the DFG-Project "Synapsen Unter Stress". For questions, send an email to Hil Meijer.

function [M,block] = Setting_variables(varargin)
%   This function assigns default values to all parameters. You can change any
%   parameter defined below with a value. Parameters down in this file that
%   are set to zero will be calculated later, and can therefore not be
%   changed.
%
%   INPUT: varargin is a vector of assigned values. an example of calling
%   this function is: Setting_variables(t0 = 10, perc=0.2) to set t0 to 10
%   and perc to 0.2.
%
%   OUTPUT: a dictionary with all parameters and their assigned values.

%Ensure the input has the right format; It might fail for functions of functions otherwise
if length(varargin) ==1
    varargin = varargin{1};
end

%Put all the default values in a dictionary M.
M = dictionary;

%--------------------------------------------------------------
%---------CURRENT STRENGTHS AND BLOCKADES----------------------
%--------------------------------------------------------------

        %Current strengths
M('pumpScaleAst')=       1.0;            % baseline astrocyte pump strength factor (Pscale)
M('pumpScaleNeuron')=    2.0;            % baseline neuron pump strength factor (Pscale)
M('nkccScale')=          1.0;            % factor NKCC1 flux rate
M('kirScale')=           1.0;            % factor Kir conductance
M('eaatScaleNeuron')=    1.0;            % factor EAAT conductance (neuron)
M('eaatScaleAst')=       1.0;            % factor EAAT conductance (astrocyte)
%1-5-24 HGE: neuronal EAAT is 10% of astrocyte EAAT (ie ratio 1:9), so we set only paEEAT
M('paEAAT')=             1e-6;           % strength of EAAT in the astrocyte
M('pnEAAT')=             M('paEAAT')/9;  % strength of EAAT in the neuron
M('KCCscale')=           1.0;
M('ncxScale')=           1.0;
M('nka_na')=             13.0;           % Half saturation concentration for intracellular Na+
M('nka_k') =             0.2;            % Half saturation concentration for extracellular K+
M('nka_na_g')=           13.0;           % Half saturation concentration for glial Na+
M('nka_k_g')=            0.2;            % Half saturation concentration for extracellular K+ (glia)

        %NKA blockade
M('beta1')=              4;              % sigmoidal rate NKA blockade onset
M('beta2')=              4;              % sigmoidal rate NKA blockade offset
M('perc')=               0.0;            % Available energy during NKA blockade (in fraction, between 0 and 1)
M('tstart')=             100.0;          % Time of NKA blockade onset (in min.)
M('tend')=               120.0;          % Time of NKA blockade offset (in min.)

        % Other dynamics
M('excite')=             0;              % enter a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY] to excite the neuron with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY. Values are saved as "excite 1" upto "excite 5"
M('excite2')=            0;              % Usefull if you want a second excitation.  % either 0 (no excitation), or a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY]. Excite the neuron with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY Values are saved as "excite2 1" upto "excite2 5"
M('excite3')=            0;              % Usefull if you want a third excitation.  % either 0 (no excitation), or a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY]. Excite the neuron with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY Values are saved as "excite2 1" upto "excite2 5"
M('exciteK')=            0;              % enter a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY] to add K into the astrocyte with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY. Values are saved as "exciteK 1" upto "exciteK 5"
M('exciteK2')=           0;              % enter a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY] to add K into the astrocyte with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY. Values are saved as "exciteK 1" upto "exciteK 5"
M('InjectCaNeuron')=     0;              % enter a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY] to add Ca into the Neuron with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY. Values are saved as "InjectCaNeuron 1" upto "InjectCaNeuron 5"
M('InjectCaAst')=        0;              % enter a vector of 1x5 of [TSTART TFINAL CURRENT WAVELENGTH PULSEDUTY] to add Ca into the astrocyte with (CURRENT)pA current starting at TSTART and ending at TEND with wavelength WAVELENGTH and duty DUTY. Values are saved as "InjectCaAst 1" upto "InjectCaAst 5"
M('astblock')=           0;              % enter either 0 (normal functioning astrocyte) or vector [TSTART TFINAL]. block all astrocyte interactions starting at TSTART and ending at TEND. Values are saved as "astblock 1" upto "astblock 5"
M('block')=              0;              % either 0 (no block) or a vector of vectors [["BLOCK1", starttime, endtime, minimum functionality between 0 and 1]; ["BLOCK2", starttime, endtime, minimum functionality between 0 and 1], ...]. You can choose "BLOCK" from the currents in lines 283 to 309 in the file Model,m
M('nogates')=            0;              % Value of 0 (gates used), or 1 (gates not used). Makes the simulation much faster by setting m,h and n to their equilibrium values, useful if you only want to see ion concentration dynamics 
M('nosynapse')=          0;              % Value of 0 (synapse used), or 1 (synapse not used). Blocks all synaptic processes (glutamate recyling, GLT, NCX). Use if you are interested only in somatic dynamics
M('nochargecons')=       0;              % Value of 0 (charge conservation) or 1 (ignore charge conservation). This removes NBe and might be useful to adjust baseline equilibria)
M('block_graduallity') = 100;            % Value to make the onset of the block more or less gradual. Plot 'BlockOther' to see if the block takes place fully. 
M('block_graduallity2')= 100;            % Value to make the solving of the block more or less gradual. Plot 'BlockOther' to see if the block takes place fully. 
M('excite_steepness')  = 100;            % Value to make the excite more steep.  

        %----------------------------------------------------------
        %---------GENERAL SIMULATION SETTINGS----------------------
        %----------------------------------------------------------

M('t0')=                 0.0;            % Start time of simulation (in min.)
M('tfinal')=             150;            % End time of simulation (in min.)
M('alphae0')=            0;              % Initial extracellular volume ratio (in fraction, between 0 and 1)
M('ECSsizes')=           's';            % either 's' (small), 'm' (medium) or 'b' (big). default is small.
M('perc_gray')=          0.95;           % Gray area in plotting starts when avaliable energy is below perc_gray

        %-----------------------------------------------
        %---------FIXED PARAMETERS----------------------
        %-----------------------------------------------

M('C')=                  20.0;           % Neuron membrane capacitance
M('F') =                 96485.333;      % Faraday's constant
M('R') =                 8314.4598;      % Gas constant
M('T') =                 310.0;          % Temperature

M('HeOHa')=              0.66;           % Proton ratio (ex:in) in astrocytes (fixed)
M('HeOHai')=             0.66;           % Proton ratio (ex:in) in neurons (fixed)

        %gated currents
M('PNaG') =              80*1e-5;        % permeability of gated Na current
M('PKG') =               40*1e-5;        % permbeability of gated K current
M('PClG') =              1.95*1e-5;      % permeability of gated Cl current
%HGE Adjusting this one to add more Ca into the cell
M('PCaG')=               2e-6;           % permeability of voltage-gated Ca channel

        %leak currents
M('PNaL_base')=          0.2*1e-5;       % permeability of Na leak current
M('PKL_base')=           2*1e-5;         % permeability of K leak current
M('PClL_base')=          0.25*1e-5;      % permeability of Cl leak current

        %KCL current
M('UKCl')=               13*1e-7;        % flux rate of KCl cotransporter in neuron
M('UKClg')=              13*1e-7;        % flux rate of KCL cotransporter in glia

        % Water osmosis
M('LH20i')=              2*1e-14 ;       % Osmotic permeability in the neuron

        %NKA pump
M('PNKAi')=              54.5;       % Baseline NKA pump strength

        %membrane capacitance/potential
M('Cg')=                 20.0;           % Astrocyte membrane capacitancexf
M('Vg0') =               -80.0;          % Fix initial glial membrane potential
M('Vi0')=                -65.5;          % Fix initial neuronal membrane potential

        %Kir gate
M('KCe_thres') =         13.0;           % Kir: Threshold for Kir gate
M('kup2') =              0.1;            % Kir: Rate of transition from low uptake to high uptake

        %NCX current
% M('PNCXi')=              10.8;         % old % NCX conductance (defined in tps/fm_params.py)
% M('PNCXg')=              5.7;          % old % Astrocyte NCX conductance (defined in tps/fm_params.py)
M('PNCXi') =             3.6;            % Neuronal NCX conductance, 1/15 of NKA strength
M('alphaNaNCX')=         87.5;           % NCX: Na half saturation concentration
M('alphaCaNCX')=         1.38;           % NCX: Ca  half saturation concentration
M('eNCX')=               0.35;           % NCX: position of energy barrier
M('ksatNCX')=            0.1;            % NCX: saturation factor
M('PNCXg')=              5.7;            % Astrocyte NCX conductance (identical to neuron)


        %EAAT current
M('PEAATi')=             0;              % EAAT: Neuronal EAAT strength (defined in tps/fm_params.py)
M('EAAT_beta')=          1.2;            %  ?
M('EAAT_th')=            5*1e-4;         %  ?
M('PEAATg')=             0;              % EAAT: Astrocyte EAAT strength (defined in tps/fm_params.py)

        %Glutamate recycling
M('k1max')=              1;              % Glu recycling: Max forward reaction rate
M('KM')=                 0.0023;         % Glu recycling: Ca half-saturation concentration
M('KDV') =               0.1;            % Glu recycling: Half saturation for forward reaction rate
M('k20')=                0.021*1e-3;     % Glu recycling: Uncatalysed forward reaction rate
M('k2cat')=              20*1e-3;        % Glu recycling: Catalysed forward reaction rate
M('kmin20')=             0.017*1e-3;     % Glu recycling: Uncatalysed backward reaction rate
M('kmin1')=              0.05*1e-3;      % Glu recycling: Backward reaction rate
M('k3')=                 4.4;            % Glu recycling: Forward reaction rate
M('kmin3')=              56*1e-3;        % Glu recycling: Backward reaction rate
M('k4')=                 1.45;           % Glu recycling: Fusion rate
M('trec')=               10;             % Glu recycling: Vesicle fusion factor


        %---------------------------------------------------------------------------------
        %---------INITIAL CONCENTRATIONS AND VOLUMES (BASELINE REST)----------------------
        %---------------------------------------------------------------------------------
        % initial ion concentrations
M('NaCi0')=              13;             % Neuronal sodium concentration
M('KCi0')=               145;            % Neuronal potassium concentration
M('ClCi0')=              7;              % Neuronal chloride concentration
M('CaCi0')=              0.1*1e-3;       % Neuronal calcium concentration
M('NaCe0')=              152;            % Extracellular sodium concentration
M('KCe0')=               3;              % Extracellular potassium concentration
M('ClCe0')=              135;            % Extracellular chloride concentration
M('CaCc0')=              1.8;            % Cleft calcium concentration
M('GluCc0')=             1*1e-4;         % Cleft glutamate concentration
M('NaCg0')=              13;             % Astrocyte sodium concentration
M('KCg0')=               80;             % Astrocyte potassium concentration
M('ClCg0')=              35;             % Astrocyte chloride concentration
M('CaCg0')=              0.11*1e-3;      % Astrocyte calcium concentration
M('GluCi0')=             2;             % Neuronal glutamate concentration
M('GluCg0')=             2.0;              % Astrocyte glutamate concentration
%HGE 15-4-24; test lowering these values from 2.0 to ..
% M('GluCg0')=             1.0;              % Astrocyte glutamate concentration

        %initial volumes
M('Wi0')=                2;              % Neuronal volume
M('Wg0')=                1.7;            % Astrocyte volume
M('VolPreSyn')=          1*1e-3;         % Presynaptic terminal volume (fixed)
M('VolPAP')=             1*1e-3;         % Perisynaptic astrocyte process volume (fixed)
M('Volc')=               1*1e-3;         % Cleft volume (fixed)

        %-----------------------------------------------------------------------------------------
        %---------ALL  PARAMETERS BELOW ARE COMPUTED/DEFINED IN ANOTHER FILE----------------------
        %-----------------------------------------------------------------------------------------
 
        %ion/glutamate concentrations
M('NF0')=                0;              % Fused glutamate = Cleft glutamate molar amount
M('NGlui0')=             0;              % Neuronal glutamate molar amount
M('NGluc0')=             0;              % Cleft glutamate molar amount
M('We0')=                0;              % Extracellular volume
M('NNai0')=              0;              % Neuronal sodium molar amount
M('NKi0')=               0;              % Neuronal potassium molar amount
M('NCli0')=              0;              % Neuronal chloride molar amount
M('NCai0')=              0;              % Neuronal calcium molar amount
M('NNae0')=              0;              % Extracellular sodium molar amount
M('NKe0')=               0;              % Extracellular potassium molar amount
M('NCle0')=              0;              % Extracellular chloride molar amount
M('NCac0')=              0;              % Cleft calcium molar amount
M('NNag0')=              0;              % Astrocyte sodium molar amount
M('NKg0')=               0;              % Astrocyte potassium molar amount
M('NClg0') =             0;              % Astrocyte chloride molar amount
M('NCag0')=              0;              % Astrocyte calcium molar amount
M('NGlug0')=             0;              % Astrocyte sodium molar amount
M('CNa')=                0;              % Total molar amount of sodium in the system
M('CK')=                 0;              % Total molar amount of potassium in the system
M('CCl')=                0;              % Total molar amount of chloride in the system
M('CCa')=                0;              % Total molar amount of calcium in the system

        %volumes
M('Wtot')=               0;              % Total volume in the system
M('NAi')=                0;              % Molar amount of neuronal impermeant anions
M('NAe')=                0;              % Molar amount of extracellular impermeant anions
M('NBe')=                0;              % Molar amount of extracellular impermeant cations
M('NAg')=                0;              % Molar amount of astrocyte impermeant anions
M('NBg')=                0;              % Molar amount of astrocyte impermeant cations
        %Gates
M('alpham0')=            0;              % Gating: alpha function for m for Vi=Vi0
M('betam0')=             0;              % Gating: beta function for m for Vi=Vi0
M('alphah0')=            0;              % Gating: alpha function for h for Vi=Vi0
M('betah0')=             0;              % Gating: beta function for h for Vi=Vi0
M('alphan0')=            0;              % Gating: alpha function for n for Vi=Vi0
M('betan0')=             0;              % Gating: beta function for n for Vi=Vi0
M('m0')=                 0;              % Gating: baseline sodium activation
M('h0')=                 0;              % Gating: baseline sodium inactivation
M('n0')=                 0;              % Gating: baseline potassium activation
        % Neuronal leaks
M('INaGi0')=             0;              % Baseline neuronal sodium voltage gated channel current
M('IKGi0')=              0;              % Baseline neuronal potassium voltage gated channel current
M('IClGi0')=             0;              % Baseline neuronal chloride voltage gated channel current
M('INaLi0')=             0;              % Leak: baseline neuronal sodium leak
M('IKLi0')=              0;              % Leak: baseline neuronal potassium leak
M('IClLi0')=             0;              % Leak: baseline neuronal chloride leak
M('JKCl0')=              0;              % Baseline KCC-cotrasnporter flux
M('sigmapump')=          0;              % NKA: sigma expression at baseline in neuron
M('fpump')=              0;              % NKA: g expression at baseline in neuron
M('neurPump')=           0;              % NKA: total pump current at baseline in neuron
M('INCXi0')=             0;              % NCX: baseline neuronal current
M('JEAATi0')=            0;              % EAAT: baseline neuronal current
M('ICaG0')=              0;              % Voltage-gated calcium channel: baseline neuronal current
M('ICaLi0')=             0;              % Leak: baseline neuronal calcium leak
M('IGluLi0')=            0;              % Leak: baseline neuronal glutamate leak
M('PNaLi')=              0;              % sodium leak conductance in neuron
M('PKLi')=               0;              % potassium leak conductance in neuron
M('PClLi')=              0;              % chloride leak conducatance in neuron
M('PCaLi')=              0;              % caclium leak conductance in neuron
        % Glial uptake parameters
M('PNKAg')=              0;              % NKA: astrocyte conductance
M('LH20g')=              0;              % Astrocyte water permeability
M('PNKCC1')=             0;              % NKCC1: astrocyte conductance
M('PKir')=               0;              % Kir4.1: astrocyte conductance
        %-----------------------------------------------------------------------------------------------
        % Astrocyte leaks
M('IKLg0')=              0;              % Baseline astrocyte potassium leak
M('IClLg0')=             0;              % Baseline astrocyte chloride leak
M('INaLg0')=             0;              % Baseline astrocyte sodium leak
M('JNKCC10')=            0;              % NKCC1: Baseline astrocyte flux
M('sigmapumpA')=         0;              % NKA: sigma expression at baseline for astrocyte
M('fpumpA')=             0;              % NKA: g expression at baseline for astrocyte
M('astpump')=            0;              % NKA: total pump current at baseline for astrocyte
M('IKir0')=              0;              % Baseline Kir channel current for astrocyte
M('IGluLg0')=            0;              % Baseline astrocyte glutamate leak
M('ICaLg0')=             0;              % Baseline astrocyte calcium leak
M('JEAATg0')=            0;              % Baseline astrocyte EAAT current
M('vINCXg0')=            0;              % Baseline astrocyte NCX current
M('k1init')=             0;              % Glu recycling: k1 at baseline
M('gCainit')=            0;              % Glu recycling: gCa at baseline
M('k2init')=             0;              % Glu recycling: k2 at baseline
M('kmin2catinit')=       0;              % Glu recycling: kmin2cat at baseline
M('kmin2init')=          0;              % Glu recycling: kmin2 at baseline
M('PNaLg')=              0;              % Astrocyte sodium leak conductance
M('PKLg')=               0;              % Astrocyte potassium leak conductance
M('PClLg')=              0;              % Astrocyte chloride leak conductance
M('PCaLg')=              0;              % Astrocyte calcium leak conductance
        %---------------------------------------------------------------------------------------------------------
        %Glutamate recycling initial conditions
M('NI0')=                0;              % Molar amount of inactive=presynaptic glutamate
M('ND0')=                0;              % Molar amount of depot (D)
M('NN0')=                0;              % Molar amount of non releasable pool (N)
M('NR0')=                0;              % Molar amount of readily releasable pool (R)
M('NR10')=               0;              % Molar amount of readily releasable pool 1 (R1)
M('NR20')=               0;              % Molar amount of readily releasable pool 2 (R2)
M('NR30')=               0;              % Molar amount of readily releasable pool 3 (R3)
M('PGluLi')=             0;              % Astrocyte glutamate leak conductance
M('PGluLg')=             0;              % Neuron glutamate leak conductance
M('CGlu')=               0;              % Molar amount of total glutamate in the system

M('BE')=                 0;              % Strength of bath Exchange

block=[];
if length(varargin)>1
for i=1:2:length(varargin)              % Assign input values to parameters
    varargin{i}; %#ok<VUNUS>
    M(varargin{i}) ;                    % If you get an error, one of the labels is misspelled.
    if  varargin{i}=="block"            % A dictionary cannot save strings, so I will save block separately.
        M("block")=1;
        block=[block,varargin(i+1)]; %#ok<AGROW>
    elseif length(varargin{i+1})>1
        M(varargin{i})= 1;               %the specific key is turned on
        for j=1:length(varargin{i+1})
        key = join([varargin{i}, j],'_');    
        M(key)= varargin{i+1}(j);       %The different parts of value are saved under 'key 1', 'key 2', ..., 'key n'
        end
    else
        M(varargin{i})= varargin{i+1};      % Assign new values (varargin{i+1}) to labels varargin{i}
    end
end

%%Adjusting time of tstart and tend of energy deprivation such that start
%%and endtime correspond with the graph
M('tstart_old')=M('tstart');
M('tend_old')=M('tend');
% M('tstart') = M('tstart_old') - 1/M('beta1')*log(1/M('perc_gray') -1);
% M('tend') = M('tend_old') + 1/M('beta2')*log(1/M('perc_gray') - 1);

end


% All parameters in another format: 
% pumpScaleAst=       1.0,            % baseline astrocyte pump strength factor (Pscale)
% pumpScaleNeuron=    1.0,            % baseline neuron pump strength factor (Pscale)
% nkccScale=          1.0,            % factor NKCC1 flux rate
% kirScale=           1.0,            % factor Kir conductance
% eaatScaleNeuron=    1.0,            % factor EAAT conductance (neuron)
% eaatScaleAst=       1.0,            % factor EAAT conductance (astrocyte)
% pnEAAT=             1e-6,           % FIM van Alphen: strength of EAAT in the neuron
% paEAAT=             2*1e-5,         % FIM van Alphen strength of EAAT in the astrocyte
% KCCscale=           1.0)
% ncxScale=           1.0,
% nka_na=             13.0,
% nka_k =             0.2,
% nka_na_g=           13.0,
% nka_k_g=            0.2,
% beta1=              4,              % sigmoidal rate NKA blockade onset
% beta2=              4,              % sigmoidal rate NKA blockade offset
% perc=               0.0,            % Available energy during NKA blockade (in fraction, between 0 and 1)
% tstart=             20.0,           % Time of NKA blockade onset (in min.)
% tend =              80.0,           % Time of NKA blockade offset (in min.)
% t0=                 0.0,                   
% tfinal=             150,            % End time of simulation (in min.)
% alphae0=            0.0,            % Initial extracellular volume ratio (in fraction, between 0 and 1)
%         %-----------------------------------------------
%         %---------FIXED PARAMETERS----------------------
%         %-----------------------------------------------
%         
% C=                  20.0,           % Neuron membrane capacitance
% F =                 96485.333,      % Faraday's constant
% R =                 8314.4598,      % Gas constant
% T =                 310.0,          % Temperature
% PNaG =              80*1e-5,        % permeability of gated Na current
% PKG =               40*1e-5,        % permbeability of gated K current
% PClG =              1.95*1e-5,      % permeability of gated Cl current
% PNaL_base=          0.2*1e-5,               
% PKL_base=           2*1e-5,
% PClL_base=          0.25*1e-5,
% UKCl=               13*1e-7,        % flux rate of KCl cotransporter
% UKClg=              13*1e-7,
% LH20i=              2*1e-14 ,       % Osmotic permeability in the neuron
% PNKAi=              54.5*1.6,       % Baseline NKA pump strength
% Cg=                 20.0,           % Astrocyte membrane capacitance
% Vg0 =               -80.0,          % Fix initial glial membrane potential
% Vi0=                -65.5,          % Fix initial neuronal membrane potential
% KCe_thres =         13.0,           % Kir: Threshold for Kir gate
% kup2 =              0.1,            % Kir: Rate of transition from low uptake to high uptake
% PCaG=               2*0.75*1e-5,    % permeability of voltage-gated Ca channel
% PNCXi=              0,              % NCX conductance (defined in tps/fm_params.py)
% alphaNaNCX=         87.5,           % NCX: Na half saturation concentration
% alphaCaNCX=         1.38,           % NCX: Ca  half saturation concentration
% eNCX=               0.35,           % NCX: position of energy barrier
% ksatNCX=            0.1,            % NCX: saturation factor
% PEAATi=             0,              % EAAT: Neuronal EAAT strength (defined in tps/fm_params.py)
% EAAT_beta=          1.2,
% EAAT_th=            5*1e-4,
% PEAATg=             0,              % EAAT: Astrocyte EAAT strength (defined in tps/fm_params.py)
% HeOHa=              0.66,           % Proton ratio (ex:in) in astrocytes (fixed)
% HeOHai=             0.66,           % Proton ratio (ex:in) in neurons (fixed)
% k1max=              1,              % Glut recycling: Max forward reaction rate
% KM=                 0.0023,         % Glu recycling: Ca half-saturation concentration
% KDV =               0.1,            % Glu recycling: Half saturation for forward reaction rate
% k20=                0.021*1e-3,     % Glu recycling: Uncatalysed forward reaction rate
% k2cat=              20*1e-3,        % Glu recycling: Catalysed forward reaction rate
% kmin20=             0.017*1e-3,     % Glu recycling: Uncatalysed backward reaction rate
% kmin1=              0.05*1e-3,      % Glu recycling: Backward reaction rate
% k3=                 4.4,            % Glu recycling: Forward reaction rate
% kmin3=              56*1e-3,        % Glu recycling: Backward reaction rate
% k4=                 1.45,           % Glu recycling: Fusion rate
% trec=               30,             % Glu recycling: Vesicle fusion factor
% PNCXg=              0,              % Astrocyte NCX conductance (defined in tps/fm_params.py)
% perc_gray=          0.95,           % Gray area in plotting starts when avaliable energy is below perc_gray
% 
%             %Initial concentrations and volumes (baseline rest)
% NaCi0=              13,                        % Neuronal sodium concentration
% KCi0=               145,            % Neuronal potassium concentration               
% ClCi0=              7,              % Neuronal chloride concentration
% CaCi0=              0.1*1e-3,       % Neuronal calcium concentration
% GluCi0=             3 ,             % Neuronal glutamate concentration
% NaCe0=              152,            % Extracellular sodium concentration
% KCe0=               3,              % Extracellular potassium concentration
% ClCe0=              135,            % Extracellular chloride concentration
% CaCc0=              1.8,            % Cleft calcium concentration
% GluCc0=             1*1e-4,         % Cleft glutamate concentration
% NaCg0=              13,             % Astrocyte sodium concentration
% KCg0=               80,             % Astrocyte potassium concentration
% ClCg0=              35,             % Astrocyte chloride concentration
% CaCg0=              0.11*1e-3,      % Astrocyte calcium concentration
% GluCg0=             2,              % Astrocyte glutamate concentration
% Wi0=                2,              % Neuronal volume
% Wg0=                1.7,            % Astrocyte volume
% VolPreSyn=          1*1e-3,         % Presynaptic terminal volume (fixed)
% VolPAP=             1*1e-3,         % Perisynaptic astrocyte process volume (fixed)
% Volc=               1*1e-3,         % Cleft volume (fixed)
% 
%         % All parameters below are computed/defined in tps/fm_params.py 
% NF0=                0,              % Fused glutamate = Cleft glutamate molar amount
% NGlui0=             0,              % Neuronal glutamate molar amount
% NGluc0=             0,              % Cleft glutamate molar amount
% We0=                0,              % Extracellular volume
% NNai0=              0,              % Neuronal sodium molar amount
% NKi0=               0,              % Neuronal potassium molar amount
% NCli0=              0,              % Neuronal chloride molar amount
% NCai0=              0,              % Neuronal calcium molar amount
% NNae0=              0,              % Extracellular sodium molar amount  
% NKe0=               0,              % Extracellular potassium molar amount
% NCle0=              0,              % Extracellular chloride molar amount
% NCac0=              0,              % Cleft calcium molar amount
% NNag0=              0,              % Astrocyte sodium molar amount
% NKg0=               0,              % Astrocyte potassium molar amount
% NClg0 =             0,              % Astrocyte chloride molar amount
% NCag0=              0,              % Astrocyte calcium molar amount
% NGlug0=             0,              % Astrocyte sodium molar amount
% CNa=                0,              % Total molar amount of sodium in the system
% CK=                 0,              % Total molar amount of potassium in the system
% CCl=                0,              % Total molar amount of chloride in the system
% CCa=                0,              % Total molar amount of calcium in the system
% Wtot=               0,              % Total volume in the system
% NAi=                0,              % Molar amount of neuronal impermeant anions
% NAe=                0,              % Molar amount of extracellular impermeant anions
% NBe=                0,              % Molar amount of extracellular impermeant cations
% NAg=                0,              % Molar amount of astrocyte impermeant anions
% NBg=                0,              % Molar amount of astrocyte impermeant cations
%         %Gates
% alpham0=            0,              % Gating: alpha function for m for Vi=Vi0
% betam0=             0,              % Gating: beta function for m for Vi=Vi0
% alphah0=            0,              % Gating: alpha function for h for Vi=Vi0
% betah0=             0,              % Gating: beta function for h for Vi=Vi0
% alphan0=            0,              % Gating: alpha function for n for Vi=Vi0
% betan0=             0,              % Gating: beta function for n for Vi=Vi0
% m0=                 0,              % Gating: baseline sodium activation
% h0=                 0,              % Gating: baseline sodium inactivation
% n0=                 0,              % Gating: baseline potassium activation
%         % Neuronal leaks
% INaGi0=             0,              % Baseline neuronal sodium voltage gated channel current
% IKGi0=              0,              % Baseline neuronal potassium voltage gated channel current
% IClGi0=             0,              % Baseline neuronal chloride voltage gated channel current
% INaLi0=             0,              % Leak: baseline neuronal sodium leak
% IKLi0=              0,              % Leak: baseline neuronal potassium leak
% IClLi0=             0,              % Leak: baseline neuronal chloride leak
% JKCl0=              0,              % Baseline KCC-cotrasnporter flux
% sigmapump=          0,              % NKA: sigma expression at baseline in neuron
% fpump=              0,              % NKA: g expression at baseline in neuron
% neurPump=           0,              % NKA: total pump current at baseline in neuron
% INCXi0=             0,              % NCX: baseline neuronal current
% JEAATi0=            0,              % EAAT: baseline neuronal current
% ICaG0=              0,              % Voltage-gated calcium channel: baseline neuronal current
% ICaLi0=             0,              % Leak: baseline neuronal calcium leak
% IGluLi0=            0,              % Leak: baseline neuronal glutamate leak
% PNaLi=              0,              % sodium leak conductance in neuron
% PKLi=               0,              % potassium leak conductance in neuron
% PClLi=              0,              % chloride leak conducatance in neuron
% PCaLi=              0,              % caclium leak conductance in neuron
%         % Glial uptake parameters
% PNKAg=              0,              % NKA: astrocyte conductance
% LH20g=              0,              % Astrocyte water permeability
% PNKCC1=             0,              % NKCC1: astrocyte conductance
% PKir=               0,              % Kir4.1: astrocyte conductance
%         %-----------------------------------------------------------------------------------------------
%         % Astrocyte leaks
% IKLg0=              0,              % Baseline astrocyte potassium leak
% IClLg0=             0,              % Baseline astrocyte chloride leak
% INaLg0=             0,              % Baseline astrocyte sodium leak
% JNKCC10=            0,              % NKCC1: Baseline astrocyte flux
% sigmapumpA=         0,              % NKA: sigma expression at baseline for astrocyte
% fpumpA=             0,              % NKA: g expression at baseline for astrocyte
% astpump=            0,              % NKA: total pump current at baseline for astrocyte
% IKir0=              0,              % Baseline Kir channel current for astrocyte
% IGluLg0=            0,              % Baseline astrocyte glutamate leak
% ICaLg0=             0,              % Baseline astrocyte calcium leak
% JEAATg0=            0,              % Baseline astrocyte EAAT current
% INCXg0=             0,              % Baseline astrocyte NCX current
% k1init=             0,              % Glu recycling: k1 at baseline
% gCainit=            0,              % Glu recycling: gCa at baseline
% k2init=             0,              % Glu recycling: k2 at baseline
% kmin2catinit=       0,              % Glu recycling: kmin2cat at baseline
% kmin2init=          0,              % Glu recycling: kmin2 at baseline
% PNaLg=              0,              % Astrocyte sodium leak conductance
% PKLg=               0,              % Astrocyte potassium leak conductance
% PClLg=              0,              % Astrocyte chloride leak conductance
% PCaLg=              0,              % Astrocyte calcium leak conductance
%         %---------------------------------------------------------------------------------------------------------
%         %Glutamate recycling initial conditions
% NI0=                0,              % Molar amount of inactive=presynaptic glutamate
% ND0=                0,              % Molar amount of depot (D)
% NN0=                0,              % Molar amount of non releasable pool (N)
% NR0=                0,              % Molar amount of readily releasable pool (R)
% NR10=               0,              % Molar amount of readily releasable pool 1 (R1)
% NR20=               0,              % Molar amount of readily releasable pool 2 (R2)
% NR30=               0,              % Molar amount of readily releasable pool 3 (R3)
% PGluLi=             0,              % Astrocyte glutamate leak conductance
% PGluLg=             0,              % Neuron glutamate leak conductance
% CGlu=               0               % Molar amount of total glutamate in the system
% 

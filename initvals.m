function [t0,y0]=initvals(M)

t0 = M('t0');
y0 = [M('NNai0'), M('NKi0'), M('NCli0'), M('m0'), M('h0'), M('n0'), M('NCai0'),...
      M('NN0'), M('NR0'), M('NR10'), M('NR20'), M('NR30'), M('NI0'),M('ND0'),...
      M('NNag0'), M('NKg0'), M('NClg0'), M('NCag0'), M('NGlug0'), M('Wi0'), M('Wg0'),...
      M('NNae0'),M('NKe0'),M('NCle0'),M('NCac0'),M('NGluc0'),M('O2')]; %12-2-24 Added as these are the concentrations in the cleft communicating with the bath

if M('nogates')==0      %When the gates work, the initial values are slightly different. I do not know why.
    y0=y0 +  1e-7*[1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1].*y0;
end

end
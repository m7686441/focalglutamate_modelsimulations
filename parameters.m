function [M] = parameters(M)
%All needed parameters for M are defined in Setting variables
    if M('nosynapse')==1
        block_synapse = 0;
    elseif M('nosynapse') ==0
        block_synapse = 1;
    else
        error('Error: Value of nosynapse has not a value of 0 or 1, change this.')
    end
    if M('alphae0') == 0
        if M('ECSsizes') == 's'
            M('alphae0') = 0.2;
        elseif M('ECSsizes') == 'm'
            M('alphae0') = 0.5;
        elseif M('ECSsizes') == 'b'
            M('alphae0') = 0.98;
        else
            disp('Warning, ECSsizes does not have a value of s (small), m (medium) or b (big), so default ECS size of s (small) is used.')
            M('alphae0') = 0.2;
        end
    end


    M('PEAATg') = M('eaatScaleAst')*M('paEAAT');
    M('PEAATi') = M('eaatScaleNeuron')*M('pnEAAT');

    %---------------------------------------------------------------------------------
    %-----------------------GLIAL UPTAKE PARAMETERS-----------------------------------
    %---------------------------------------------------------------------------------
    M('PNKAg') = M('PNKAi');
    M('LH20g') = M('LH20i');
    M('PNKCC1') = M('nkccScale')*7.3215*1e-7;          % From OSTBY
    M('PKir') = M('kirScale')*0.286102;                % From Dronne

    M('NF0') = M('GluCc0')*M('Volc');
    M('NGluc0') = M('NF0');
    M('We0') = M('alphae0')*(M('Wi0') + M('Wg0'))/(1-M('alphae0'));
    M('NNai0') = M('NaCi0')*M('Wi0');
    M('NKi0') = M('KCi0')*M('Wi0');
    M('NCli0') = M('ClCi0')*M('Wi0');
    M('NCai0') = M('CaCi0')*M('VolPreSyn');
    M('NNae0') = M('NaCe0')*M('We0');
    M('NKe0') = M('KCe0')*M('We0');
    M('NCle0') = M('ClCe0')*M('We0');
    M('NCac0') = M('CaCc0')*M('Volc');
    M('NGluc0') = M('NF0');
    M('NNag0') = M('NaCg0')*M('Wg0');
    M('NKg0') = M('KCg0')*M('Wg0');
    M('NClg0') = M('ClCg0')*M('Wg0');
    M('NCag0') = M('CaCg0')*M('VolPAP');
    M('NGlug0') = M('GluCg0')*M('VolPAP');

    M('CNa') = M('NNai0') + M('NNae0') + M('NNag0');
    M('CK') = M('NKi0') + M('NKe0') + M('NKg0');
    M('CCl') = M('NCli0') + M('NCle0') + M('NClg0');
    M('CCa') = M('NCai0') + M('NCac0') + M('NCag0');
    M('Wtot') = M('Wi0') + M('We0') + M('Wg0');

    %-------------------------------------------------------------
    % Glutamate recycling initial conditions
    % -----------------------------------------------------------

    M('k1init') = M('k1max')*M('CaCi0')/(M('CaCi0')+M('KM'));
    M('gCainit') = M('CaCi0')/(M('CaCi0')+M('KDV'));
    M('k2init')= M('k20')+M('gCainit')*M('k2cat');
    M('kmin2catinit') = M('k2cat')*M('kmin20')/M('k20');
    M('kmin2init') = M('kmin20')+M('gCainit')*M('kmin2catinit');

    M('NGluitot') = M('GluCi0')*M('VolPreSyn');
    M('CGlu') = M('NGluitot') + M('NGluc0') + M('NGlug0');
    M('ND0') =  (M('CGlu')*(6*M('CaCi0')^3*M('k2init')*M('k3')^3*M('k4') + 6*M('CaCi0')^3*M('k3')^3*M('k4')*M('kmin1') + 2*M('CaCi0')^2*M('k3')^2*M('k4')*M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3')*M('k4')*M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4')*M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) - 6*M('CaCi0')^3*M('k1init')*M('k2init')*M('k3')^3*M('k4')*M('trec') - M('NGluc0')*(6*M('CaCi0')^3*M('k2init')*M('k3')^3*M('k4') + 6*M('CaCi0')^3*M('k3')^3*M('k4')*M('kmin1') + 2*M('CaCi0')^2*M('k3')^2*M('k4')*M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3')*M('k4')*M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4')*M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) - M('NGlug0')*(6*M('CaCi0')^3*M('k2init')*M('k3')^3*M('k4') + 6*M('CaCi0')^3*M('k3')^3*M('k4')*M('kmin1') + 2*M('CaCi0')^2*M('k3')^2*M('k4')*M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3')*M('k4')*M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3))/(6*M('CaCi0')^3*M('k1init')*M('k2init')*M('k3') ^3 + 6*M('CaCi0')^3*M('k1init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 11*M('CaCi0')^2*M('k1init')*M('k2init')*M('k3') ^2*M('k4') + 18*M('CaCi0')^2*M('k1init')*M('k2init')*M('k3') ^2*M('kmin3') + 2*M('CaCi0')^2*M('k1init')*M('k3') ^2*M('k4') *M('kmin2init') + 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + 7*M('CaCi0')*M('k1init')*M('k2init')*M('k3') *M('k4') *M('kmin3') + 18*M('CaCi0')*M('k1init')*M('k2init')*M('k3') *M('kmin3')^2 + M('CaCi0')*M('k1init')*M('k3') *M('k4') *M('kmin2init')*M('kmin3') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k1init')*M('k2init')*M('k4') *M('kmin3')^2 + 6*M('k1init')*M('k2init')*M('kmin3')^3 + 2*M('k1init')*M('k4') *M('kmin2init')*M('kmin3')^2 + 6*M('k1init')*M('kmin2init')*M('kmin3')^3 + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3);
    M('NI0') = 6*M('CaCi0')^3*M('k1init')*M('k2init')*M('k3') ^3*M('k4') *M('trec')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3);
    M('NN0') = (-6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + M('ND0')*M('k1init'))/M('kmin1');
    M('NR0') = (-6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4') *M('kmin1')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + M('k2init')*(-6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + M('ND0')*M('k1init')))/(M('kmin1')*M('kmin2init'));
    M('NR10') = (-18*M('CaCi0')^4*M('ND0')*M('k1init')*M('k2init')*M('k3') ^4*M('k4') *M('kmin1')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3')^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) - 6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4') *M('kmin1')*M('kmin2init') /(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + 3*M('CaCi0')*M('k2init')*M('k3') *(-6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + M('ND0')*M('k1init')))/(M('kmin1')*M('kmin2init')*M('kmin3'));
    M('NR20') = (-18*M('CaCi0')^5*M('ND0')*M('k1init')*M('k2init')*M('k3') ^5*M('k4') *M('kmin1')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3')^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) - 6*M('CaCi0')^4*M('ND0')*M('k1init')*M('k2init')*M('k3') ^4*M('k4') *M('kmin1')*M('kmin2init') /(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) - 3*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + 3*M('CaCi0')^2*M('k2init')*M('k3') ^2*(-6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3*M('k4')/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3) + M('ND0')*M('k1init')))/(M('kmin1')*M('kmin2init')*M('kmin3')^2);
    M('NR30') = 6*M('CaCi0')^3*M('ND0')*M('k1init')*M('k2init')*M('k3') ^3/(6*M('CaCi0')^3*M('k2init')*M('k3') ^3*M('k4') + 6*M('CaCi0')^3*M('k3') ^3*M('k4') *M('kmin1')+ 2*M('CaCi0')^2*M('k3') ^2*M('k4') *M('kmin1')*M('kmin2init') + M('CaCi0')*M('k3') *M('k4') *M('kmin1')*M('kmin2init')*M('kmin3') + 2*M('k4') *M('kmin1')*M('kmin2init')*M('kmin3')^2 + 6*M('kmin1')*M('kmin2init')*M('kmin3')^3);
    M('NGlui0') = M('NGluitot');

    % Impermeants and conserved quantities
    M('NAi') = (block_synapse*2*M('NCai0') - M('NCli0') - ....
             block_synapse*M('NGlui0') + M('NKi0') + M('NNai0') - (M('C')*M('Vi0'))/M('F'));
    M('NAe') = (M('Cg')*M('Vg0')*M('Wi0') + M('C')*M('Vi0')*(-M('We0') + M('Wi0')) + ...
             M('F')*(block_synapse*2*M('NCai0')*M('We0') - ...
                  block_synapse*M('NGlui0')*M('We0') + 2*M('NKi0')*M('We0') + ...
                  2*M('NNai0')*M('We0') + block_synapse*2*M('NCac0')*M('Wi0') - ...
                  2*M('NCle0')*M('Wi0') - ...
                  block_synapse*M('NGluc0')*M('Wi0')))/(2*M('F')*M('Wi0')) ;
    M('NBe') = -((M('Cg')*M('Vg0')*M('Wi0') + ...
               M('C')*M('Vi0')*(M('We0') + M('Wi0')) + ...
               M('F')*(-block_synapse*2*M('NCai0')*M('We0') + ...
                    block_synapse*M('NGlui0')*M('We0') - ...
                    2*M('NKi0')*M('We0') - 2*M('NNai0')*M('We0') + ...
                    block_synapse*2*M('NCac0')*M('Wi0') - ...
                    block_synapse*M('NGluc0')*M('Wi0') + ...
                    2*M('NKe0')*M('Wi0') + 2*M('NNae0')*M('Wi0')))/(2*M('F')*M('Wi0')));
    M('NAg') = -((M('C')*M('Vi0')*M('Wg0') + ...
               M('Cg')*M('Vg0')*M('Wi0') + ...
               M('F')*(-block_synapse*2*M('NCai0')*M('Wg0') + ...
                    block_synapse*M('NGlui0')*M('Wg0') - ...
                    2*M('NKi0')*M('Wg0') - 2*M('NNai0')*M('Wg0') - ...
                    block_synapse*2*M('NCag0')*M('Wi0') + ...
                    2*M('NClg0')*M('Wi0') + ...
                    block_synapse*M('NGlug0')*M('Wi0')))/(2*M('F')*M('Wi0')));
    M('NBg') = (-M('C')*M('Vi0')*M('Wg0') + ...
             M('Cg')*M('Vg0')*M('Wi0') + ...
             M('F')*(block_synapse*2*M('NCai0')*M('Wg0') - ...
                  block_synapse*M('NGlui0')*M('Wg0') + 2*M('NKi0')*M('Wg0') + ...
                  2*M('NNai0')*M('Wg0') - block_synapse*2*M('NCag0')*M('Wi0') + ...
                  block_synapse*M('NGlug0')*M('Wi0') - 2*M('NKg0')*M('Wi0') - ...
                  2*M('NNag0')*M('Wi0')))/(2*M('F')*M('Wi0'));

    % If we ignore charge conservation and thus remove NBe (this might be useful
    % to adjust baseline equilibria)
    if M('nochargecons') == 1
        M('NAe') = -((M('C')*M('Vi0')*M('We0') + ...
                   M('F')*(-2*M('NCai0')*M('We0') + M('NGlui0')*M('We0') - ...
                        2*M('NKi0')*M('We0') - 2*M('NNai0')*M('We0') + ...
                        M('NCle0')*M('Wi0') + M('NKe0')*M('Wi0') + ...
                        M('NNae0')*M('Wi0')))/(M('F')*M('Wi0')));
        M('NBe') = 0;
    end

    M('NGlui0') = M('NI0');
    M('GluCi0') = M('NGlui0')/M('VolPreSyn');

    % Gates
    M('alpham0') = 0.32*(M('Vi0')+52)/(1-exp(-(M('Vi0')+52)/4));
    M('betam0') = 0.28*(M('Vi0')+25)/(exp((M('Vi0')+25)/5)-1);
    M('alphah0') = 0.128*exp(-(M('Vi0')+53)/18);
    M('betah0') = 4/(1+exp(-(M('Vi0')+30)/5));
    M('alphan0') = 0.016*(M('Vi0')+35)/(1-exp(-(M('Vi0')+35)/5));
    M('betan0') = 0.25*exp(-(M('Vi0')+50)/40);
    M('m0') = M('alpham0')/(M('alpham0')+M('betam0'));
    M('h0') = M('alphah0')/(M('alphah0')+M('betah0'));
    M('n0') = M('alphan0')/(M('alphan0')+M('betan0'));

    % Neuronal leaks
    M('INaG0') = M('PNaG')*(M('m0')^3)*(M('h0'))*(M('F')^2)*(M('Vi0'))/( ...
        M('R')*M('T'))*((M('NaCi0') - ...
                   M('NaCe0')*exp(-(M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
                       1-exp(-(M('F')*M('Vi0'))/(M('R')*M('T')))));
    M('IKG0') = (M('PKG')*(M('n0')^2))*(M('F')^2)*(M('Vi0'))/( ...
        M('R')*M('T'))*((M('KCi0') - ...
                   M('KCe0')*exp(-(M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
                       1-exp(-M('F')*M('Vi0')/(M('R')*M('T')))));

    M('IClG0') = M('PClG')*1/(1+exp(-(M('Vi0')+10)/10))*(M('F')^2)*M('Vi0')/( ...
        M('R')*M('T'))*((M('ClCi0') - ...
                   M('ClCe0')*exp(M('F')*M('Vi0')/(M('R')*M('T'))))/( ...
                       1-exp(M('F')*M('Vi0')/(M('R')*M('T')))));
    M('INaLi0') = (M('F')^2)/(M('R')*M('T'))*M('Vi0')*(( ...
        M('NaCi0') - ...
        M('NaCe0')*exp((-M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
            1-exp((-M('F')*M('Vi0'))/(M('R')*M('T')))));
    M('IKLi0') = M('F')^2/(M('R')*M('T'))*M('Vi0')*(( ...
        M('KCi0')-M('KCe0')*exp((-M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
            1-exp((-M('F')*M('Vi0'))/(M('R')*M('T')))));
    M('IClLi0') = (M('F')^2)/(M('R')*M('T'))*M('Vi0')*(( ...
        M('ClCi0')-M('ClCe0')*exp((M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
            1-exp((M('F')*M('Vi0'))/(M('R')*M('T')))));
    M('JKCl0') = M('UKCl')*M('R')*M('T')/M('F')*(log(M('KCi0')) + ...
                                  log(M('ClCi0'))-log(M('KCe0'))-log(M('ClCe0')));
    M('sigmapump') = 1/7*(exp(M('NaCe0')/67.3)-1);
    M('fpump') = 1/(1+0.1245*exp(-0.1*M('F')/M('R')/M('T')*M('Vi0')) + ...
                 0.0365*M('sigmapump')*exp(-M('F')/M('R')/M('T')*M('Vi0')));
    M('neurPump') = M('pumpScaleNeuron')*M('PNKAi')*M('fpump')*(M('NaCi0')^(1.5)/( ...
        M('NaCi0')^(1.5)+M('nka_na')^1.5))*(M('KCe0')/(M('KCe0')+M('nka_k')));
    M('INCXi0') = M('PNCXi')*(M('NaCe0')^3)/( ...
        M('alphaNaNCX')^3+M('NaCe0')^3)*( ...
            M('CaCc0')/(M('alphaCaNCX')+M('CaCc0')))*( ...
                M('NaCi0')^3/M('NaCe0')^3*exp(M('eNCX')*M('F')*M('Vi0')/M('R')/M('T')) - ...
                M('CaCi0')/M('CaCc0')*exp((M('eNCX')-1)*M('F')*M('Vi0')/M('R')/M('T')))/( ...
                    1+M('ksatNCX')*exp((M('eNCX')-1)*M('F')*M('Vi0')/M('R')/M('T')));
    M('JEAATi0') = M('PEAATi')*M('R')*M('T')/M('F')*log( ...
        M('NaCe0')^3/M('NaCi0')^3*M('KCi0')/M('KCe0')*M('HeOHai')*M('GluCc0')/M('GluCi0')); % Option 1: Cotransporter activation gate
    M('ICaG0') = M('PCaG')*M('m0')^2*M('h0')*4*M('F').^2/(M('R')*M('T'))*M('Vi0')*(( ...
        M('CaCi0')-M('CaCc0')*exp(-2*(M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
            1-exp(-2*(M('F')*M('Vi0'))/(M('R')*M('T')))));
    M('ICaLi0') = 4*(M('F')^2)/(M('R')*M('T'))*M('Vi0')*(( ...
        M('CaCi0')-M('CaCc0')*exp((-2*M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
            1-exp((-2*M('F')*M('Vi0'))/(M('R')*M('T')))));
    M('IGluLi0') = M('F')^2/(M('R')*M('T'))*M('Vi0')*(( ...
        M('GluCi0')-M('GluCc0')*exp((M('F')*M('Vi0'))/(M('R')*M('T'))))/( ...
            1-exp((M('F')*M('Vi0'))/(M('R')*M('T')))));

    if M('PNaLi')                             %FIM van Alphen: The if/else are added such that I can fix one or more of the leak currents.
        fprintf('PNaLi is fixed at %.1f,\n', M('PNaLi'));
    else
        M('PNaLi') = (-M('INaG0') - 3*M('neurPump') - block_synapse*3*M('INCXi0') ...
               + block_synapse*3*M('JEAATi0')*M('F'))/M('INaLi0');  % Estimated sodium leak
    %                                                    conductance in neuron
    end
    if M('PKLi')
        fprintf('PKLi is fixed at %.1f,\n', M('PKLi'));
    else
        M('PKLi') = (-M('IKG0') + 2*M('neurPump') - M('F')*M('JKCl0') - ...
              block_synapse*M('JEAATi0')*M('F'))/M('IKLi0');    % Estimated K leak conductance in neuron
    end
    if M('PClLi')
        fprintf('PClLi is fixed at %.1f,\n', M('PClLi'));
    else
        M('PClLi') = (M('F')*M('JKCl0') - M('IClG0'))/M('IClLi0'); % Estimated Cl leak conducatance in neuron
    end
    if M('PCaLi')
        print('PCaLi is fixed at %.1f,\n', M('PCaLi'));
    else
        M('PCaLi') = (-M('ICaG0')+M('INCXi0'))/M('ICaLi0');     %Original formula from Kalia(2021)
    end

%Kept this as we used this for Kalia(2021)
    % JEAAT_g =1/(1+exp(M('EAAT_beta')*(M('EAAT_th')-M('GluCc0'))))*M('R')*M('T')/M('F')*log( ...
    %     M('NaCe0')^3/M('NaCg0')^3*M('KCg0')/M('KCe0')*M('HeOHa')*M('GluCc0')/M('GluCg0'));
    % JEAAT_g =M('R')*M('T')/M('F')*log( ...
    %     M('NaCe0')^3/M('NaCg0')^3*M('KCg0')/M('KCe0')*M('HeOHa')*M('GluCc0')/M('GluCg0'));


    % ----------------------------------------------------------------------------------
    % Astrocyte leaks
    M('IKLg0') = M('F')^2/(M('R')*M('T'))*M('Vg0')*(( ...
        M('KCg0')-M('KCe0')*exp((-M('F')*M('Vg0'))/(M('R')*M('T'))))/( ...
            1-exp((-M('F')*M('Vg0'))/(M('R')*M('T')))));
    M('IClLg0') = M('F')^2/(M('R')*M('T'))*M('Vg0')*(( ...
        M('ClCg0')-M('ClCe0')*exp((M('F')*M('Vg0'))/(M('R')*M('T'))))/( ...
            1-exp((M('F')*M('Vg0'))/(M('R')*M('T')))));
    M('INaLg0') = M('F')^2/(M('R')*M('T'))*M('Vg0')*(( ...
        M('NaCg0')-M('NaCe0')*exp((-M('F')*M('Vg0'))/(M('R')*M('T'))))/( ...
            1-exp((-M('F')*M('Vg0'))/(M('R')*M('T')))));
    M('JNKCC10') = M('PNKCC1')*M('R')*M('T')/M('F')*(log(M('KCe0')) + ...
                                      log(M('NaCe0')) + ...
                                      2*log(M('ClCe0')) - ...
                                      log(M('KCg0')) - ...
                                      log(M('NaCg0')) - 2*log(M('ClCg0')));
    M('sigmapumpA') = 1/7*(exp(M('NaCe0')/67.3)-1);
    M('fpumpA') = 1/(1+0.1245*exp(-0.1*M('F')/M('R')/M('T')*M('Vg0')) + ...
                  0.0365*M('sigmapumpA')*exp(-M('F')/M('R')/M('T')*M('Vg0')));
    M('astpump') = M('pumpScaleAst')*M('fpumpA')*M('PNKAg')*(M('NaCg0')^(1.5)/( ...
        M('NaCg0')^(1.5)+M('nka_na_g')^1.5))*( ...
            M('KCe0')/(M('KCe0')+M('nka_k_g')));
    Vkg0 = M('R')*M('T')/M('F')*log(M('KCe0')/M('KCg0'));
    minfty0 = 1/(2+exp(1.62*(M('F')/M('R')/M('T'))*(M('Vg0')-Vkg0)));
    M('IKir0') = M('PKir')*minfty0*M('KCe0')/(M('KCe0')+M('KCe_thres'))*(M('Vg0')-Vkg0);
    M('ICaLg0') = 4*M('F')^2/(M('R')*M('T'))*M('Vg0')*(( ...
        M('CaCg0')-M('CaCc0')*exp((-2*M('F')*M('Vg0'))/(M('R')*M('T'))))/( ...
            1-exp((-2*M('F')*M('Vg0'))/(M('R')*M('T')))));
    M('JEAATg0') = M('PEAATg')*M('R')*M('T')/M('F')*log( ...
            M('NaCe0')^3/M('NaCg0')^3*M('KCg0')/M('KCe0')*M('HeOHa')*M('GluCc0')/M('GluCg0'));
    M('INCXg0') = M('PNCXg')*(M('NaCe0')^3)/(M('alphaNaNCX')^3+M('NaCe0')^3)*( ...
        M('CaCc0')/(M('alphaCaNCX')+M('CaCc0')))*( ...
            M('NaCg0')^3/M('NaCe0')^3*exp(M('eNCX')*M('F')*M('Vg0')/M('R')/M('T')) - ...
            M('CaCg0')/M('CaCc0')*exp((M('eNCX')-1)*M('F')*M('Vg0')/M('R')/M('T')))/( ...
                1+M('ksatNCX')*exp((M('eNCX')-1)*M('F')*M('Vg0')/M('R')/M('T')));
    M('IGluLg0') = M('F')^2/(M('R')*M('T'))*M('Vg0')*(( ...
        M('GluCg0')-M('GluCc0')*exp((M('F')*M('Vg0'))/(M('R')*M('T'))))/( ...
            1-exp((M('F')*M('Vg0'))/(M('R')*M('T')))));

    if M('PNaLg')
        fprintf('PNaLg is fixed at %.1f,\n', M('PNaLg'));
    else
        M('PNaLg') = (-3*M('astpump') + M('F')*M('JNKCC10') ...
                -block_synapse*3*M('INCXg0')  + ...
                block_synapse*3*M('JEAATg0')*M('F'))/M('INaLg0');
        fprintf('leak Na astrocyte= %.1e,\n', M('PNaLg'));
    end

    if M('PKLg')
        fprintf('PKLg is fixed at %.1e,\n', M('PKLg'));
    else
        M('PKLg') = (M('IKir0') + 2*M('astpump') + ...
               M('F')*M('JNKCC10') -block_synapse*M('JEAATg0')*M('F') )/M('IKLg0');
        fprintf('leak K astrocyte= %.1e,\n ', M('PKLg'));
    end
    if M('PClLg')
        fprintf('PClLg is fixed at %.1e,\n', M('PClLg'));
    else
        M('PClLg') = (-2*M('F')*M('JNKCC10')) /M('IClLg0');
    end
    if M('PCaLg')
        fprintf('PCaLg is fixed at %.1e,\n', M('PCaLg'));
    else
        M('PCaLg') = (M('INCXg0'))/M('ICaLg0');
    end
    %M('kRelCa') = M('kRelCa')*(1+1e-3)
    % -------------------------------------------------------------------------------

    % Glu parameters
    if M('PGluLi')
        fprintf('PGLuLi is fixed at %.1f,\n', M('PGluLi'));
    else
        M('PGluLi') = (M('F')*M('NI0')*M('ND0')/M('trec') - M('F')*M('JEAATi0'))/M('IGluLi0');
    end
    if M('PGluLg')
        fprintf('PGluLg is fixed at %.1f,\n', M('PGluLg'));
    else
        M('PGluLg')   = -M('F')*M('JEAATg0')/M('IGluLg0');
    end
    %% MOST RECENT (our baseline is unstable for alphae0 = 80%)
    if M('alphae0') == 0.8
        M('NNai0') = 2.62555481e+01;
        M('NKi0') = 2.90164201e+02;
        M('NCli0') = 1.44199062e+01;
        M('m0') = 1.45863462e-02;
        M('h0') = 9.85797304e-01;
        M('n0') = 3.23309977e-03;
        M('NCai0') = 1.04358462e-07;
        M('NN0') = 1.53480926e-18;
        M('NR0') = 1.89122476e-18;
        M('NR10') = 4.65190283e-20;
        M('NR20') = 3.80040802e-22;
        M('NR30') = 1.07852956e-25;
        M('NI0') = 2.75071805e-03;
        M('ND0') = 1.77179868e-21;
        M('NNag0') = 2.40386841e+01;
        M('NKg0') = 1.34390376e+02;
        M('NClg0') = 5.98284913e+01;
        M('NCag0') = 1.30526963e-07;
        M('NGlug0') = 2.24922516e-03;
        M('Vi0') = -6.55000001e+01;
        M('Wi0') = 2.00265709e+00;
        M('Wg0') = 1.70208082e+00;
    end

    if M('alphae0') == 0.2
        M('NN0') = 1.53480926e-18;
        M('NR0') = 1.89122476e-18;
        M('NR10') = 4.65190283e-20;
        M('NR20') = 3.80040802e-22;
        M('NR30') = 1.07852956e-25;
        M('NI0') = 2.75071805e-03;
        M('ND0') = 1.77179868e-21;
    end

% 01-05-24 Here are all changes wrt Kalia 2021 except for the EAAT currents.
     M('PCaLi') = 1.0e-9;       % Keep this low, otherwise too much Ca at baseline.
     M('PGluLi')= 1.0e-9;       % Keep this low, otherwise unphysiological dip in extracellular glutamate upon release from neuron.
     % M('PGluLg')= 3.0e-6;     % Not too low, else EAAT reverses at baseline.
% For diffusion we add concentrations in the bath equal to the cleft/ECS
     M('NNa_B')=140.6; M('NK_B')=2.775; M('NCl_B')=124.875; M('NCa_B')=0.0018; M('NGlu_B')=1e-7;
     M('NNa_B')=M('NNae0'); M('NK_B')=M('NKe0'); M('NCl_B')=M('NCle0'); M('NCa_B')=M('NCac0'); M('NGlu_B')=1e-7;   
% oxygen dynamics from Wei 2014 ( https://doi.org/10.1152/jn.00541.2013)
     M('O2eps0')= 1.5*0.17/32*1e-3;% 1/s 1.7e-5/(100e-4)^2 Diffusion coefficient % A bit stronger to get recovery
     M('O2alpha')=5.3/32;       % g/mol conversion factor
     M('O2bath')=58;            % g/mol without activity we reach this level
     M('O2')=45.5;              % g/mol Initial condition for Extracellular oxygen
    %================================================================================
    %    CHECKS CHECKS CHECKS CHECKS CHECKS
    %=============================================================================
    
    ctr = 0;
    M('err') = 0;
    
    if (M('NAi')>0) & (M('NAe')>0) & (M('NAg')>0) & (M('NBe')>=0) & (M('NBg')>0)
        disp('Quantity of impermeants....OK')
    else
        disp('ERROR: Quantity of an impermeant is nonpositive')
        M('err') = 10;
    end
        
    if (M('PGluLg')>0) & (M('PNaLg')>0) & (M('PKLg')>0) & (M('PClLg')>0) & (M('PCaLg')>0)
        disp('Leak cond. in astrocytes....OK')
    else
        disp('ERROR: Sign error in astrocyte leak conductance')
        fprintf('PGluLg (>0): %.1e,\n', M('PGluLg'))
        fprintf('PNaLg (<0) : %.1e,\n', M('PNaLg'))
        fprintf('PKLg (<0)  : %.1e,\n', M('PKLg'))
        fprintf('PClLg (>0) : %.1e,\n', M('PClLg'))
        fprintf('PCaLg (>0) : %.1e,\n', M('PCaLg'))
        disp('----------------------------------')
        M('err') = 10;
    end
    if (M('PGluLi')>0) & (M('PNaLi')>0) & (M('PKLi')>0) & (M('PClLi')>0) & (M('PCaLi')>0)
        disp('Leak cond. in neurons....OK')
    else
        disp('ERROR: Sign error in neuron leak conductance')
        fprintf('PGluLi (>0): %.1e,\n', M('PGluLi'))
        fprintf('PNaLi (>0): %.1e,\n', M('PNaLi'))
        fprintf('PKLi (>0): %.1e,\n', M('PKLi'))
        fprintf('PClLi (>0): %.1e,\n', M('PClLi'))
        fprintf('PCaLi (>0): %.1e,\n', M('PCaLi'))
        disp('----------------------------------')
        M('err') = 10;
    end

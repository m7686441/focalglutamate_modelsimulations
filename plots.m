function []= plots(t,y,M,block,filename)
N=10;
t=t([1:N:end,end]);
y=y([1:N:end,end],:);   %plot every Nth point and the end points

if size(y,2) > 1
        NNa = y(:,1);
        NK = y(:,2);
        NCl = y(:,3);
        m = y(:,4);
        h = y(:,5);
        n = y(:,6);
        NCai = y(:,7);
        NN = y(:,8);
        NR = y(:,9);
        NR1 = y(:,10);
        NR2 = y(:,11);
        NR3= y(:,12);
        NI = y(:,13);
        ND = y(:,14);
        NNag = y(:,15);
        NKg = y(:,16);
        NClg = y(:,17);
        NCag = y(:,18);
        NGlug = y(:,19);
        Wi = y(:,20);
        Wg = y(:,21);
        NNae = y(:,22);
        NKe = y(:,23);
        NCle = y(:,24);
        NCac = y(:,25);
        NGluc = y(:,26);
        O2 = y(:,27);
else
        NNa = y(1);
        NK = y(2);
        NCl = y(3);
        m = y(4);
        h = y(5);
        n = y(6);
        NCai = y(7);
        NN = y(8);
        NR = y(9);
        NR1 = y(10);
        NR2 = y(11);
        NR3 = y(12);
        NI = y(13);
        ND = y(14);
        NNag = y(15);
        NKg = y(16);
        NClg = y(17);
        NCag = y(18);
        NGlug = y(19);
        Wi = y(20);
        Wg = y(21);
        NNae = y(22);
        NKe = y(23);
        NCle = y(24);
        NCac = y(25);
        NGluc = y(26);
        O2 = y(27);
end
if M('nosynapse')
    synapse_block = 0;
else
    synapse_block = 1;
end
        %--------------------------------------------------------------
        %---------Ionic amount and concentrations----------------------
        %--------------------------------------------------------------
%HGE: We comment some calculations below as these variables are now
%dynamic, and the quantities no longer conserved.

    % ECS
    We = M('Wtot') - Wi - Wg;
    % NNae = M('CNa') - NNag - NNa;
    % NKe = M('CK') - NKg - NK;
    % NCle = M('CCl') - NClg - NCl;
    NaCe = NNae./We;
    KCe = NKe./We;
    ClCe = NCle./We;

    % Neuron
    NGlui = NI;
    NaCi = NNa./Wi;
    KCi = NK./Wi;
    ClCi = NCl./Wi;
    CaCi = NCai./M('VolPreSyn');
    GluCi = NGlui./M('VolPreSyn');

    % Astrocyte
    NaCg = NNag./Wg;
    KCg = NKg./Wg;
    ClCg = NClg./Wg;
    GluCg = NGlug./M('VolPAP');
    CaCg = NCag./M('VolPAP');

    % Cleft
    % NCac = M('CCa') - NCai - NCag;
    % NGluc = M('CGlu') - NGlui - NGlug - ND - NN - NR - NR1- NR2 - NR3;
    CaCc = NCac./M('Volc');
    GluCc = NGluc./M('Volc');

    % Voltages
    % if M('excite') %|| M('excite2')
        %V = Vtemp
        V = M('F')./M('C').*(NNa+NK+synapse_block.*2.*NCai-synapse_block.*(NGlui+NN+NR+NR1+NR2+NR3+ND)-NCl-M('NAi'));
    % else
    %     V = M('F')./M('C').*(NNa+NK+synapse_block.*2.*NCai-synapse_block.*(NGlui+NN+NR+NR1+NR2+NR3+ND)-NCl-M('NAi'));
    % end
    Vi = V ;% Needed for plotting
    Vg = M('F')./M('Cg').*(NNag + NKg + M('NBg') - M('NAg') - NClg + ...
                   synapse_block.*2.*NCag - synapse_block.*NGlug);

    % ==========================================================================
    % --------------------NEURON------------------------------------------------
    % ==========================================================================

    % HH constants
    alpham = 0.32.*(V+52)./(1-exp(-(V+52)./4));
    betam = 0.28.*(V+25)./(exp((V+25)./5)-1);
    alphah = 0.128.*exp(-(V+53)./18);
    betah = 4./(1+exp(-(V+30)./5));
    alphan = 0.016.*(V+35)./(1-exp(-(V+35)./5));
    betan = 0.25.*exp(-(V+50)./40);

    if M('nogates')
        gates_block = 0;
        m = alpham./(alpham + betam);
        h = alphah./(alphah + betah);
        n = alphan./(alphan + betan);
    else
        gates_block = 1  ;
    end

    % Gated currents
    INaG = M('PNaG').*(m.^3).*(h).*(M('F').^2).*(V)./( ...
       M('R').*M('T')).*((NaCi - ...
                  NaCe.*exp(-(M('F').*V)./(M('R').*M('T'))))./( ...
                     1-exp(-(M('F').*V)./(M('R').*M('T')))));
    IKG = (M('PKG').*(n.^2)).*(M('F').^2).*(V)./( ...
       M('R').*M('T')).*((KCi - ...
                  KCe.*exp(-(M('F').*V)./(M('R').*M('T'))))./( ...
                     1-exp(-M('F').*V./(M('R').*M('T')))));
    IClG = M('PClG').*1./(1+exp(-(V+10)./10)).*( ...
       M('F').^2).*V./(M('R').*M('T')).*((ClCi - ...
                             ClCe.*exp(M('F').*V./(M('R').*M('T'))))./( ...
                                1-exp(M('F').*V./(M('R').*M('T')))));
    ICaG = M('PCaG').*m.^2.*h.*4.*M('F').^2./( ...
       M('R').*M('T')).*V.*((CaCi- ...
                    CaCc.*exp(-2.*(M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp(-2.*(M('F').*V)./(M('R').*M('T')))));

    % Leak currents
    INaLi = M('PNaLi').*(M('F').^2)./( ...
       M('R').*M('T')).*V.*((NaCi - ...
                    NaCe.*exp((-M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((-M('F').*V)./(M('R').*M('T')))));
    IKLi = M('PKLi').*M('F').^2./(M('R').*M('T')).*V.*((...
       KCi - ...
       KCe.*exp((-M('F').*V)./(M('R').*M('T'))))./( ...
          1-exp((-M('F').*V)./(M('R').*M('T')))));
    IClLi = M('PClLi').*(M('F').^2)./( ...
       M('R').*M('T')).*V.*((ClCi - ...
                    ClCe.*exp((M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((M('F').*V)./(M('R').*M('T')))));
    ICaLi = 4.*M('PCaLi').*(M('F').^2)./( ...
       M('R').*M('T')).*V.*((CaCi - ...
                    CaCc.*exp((-2.*M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((-2.*M('F').*V)./(M('R').*M('T')))));
    IGluLi = M('PGluLi').*M('F').^2./( ...
       M('R').*M('T')).*V.*((GluCi - ...
                    GluCc.*exp((M('F').*V)./(M('R').*M('T'))))./( ...
                       1-exp((M('F').*V)./(M('R').*M('T')))));

    % Blockade
    % blockerExp = 1./(1+exp(M('beta1').*(t-M('tstart')))) + 1./( ...
    %    1+exp(-M('beta2').*(t-M('tend'))));
    % blockerExpAlt = 1./(1+exp(M('beta1').*(t-M('tstart')))) + M('perc')./( ...
    %    1+exp(-M('beta2').*(t-M('tend'))));
    % blockerExp = M('perc') + (1-M('perc')).*blockerExp;
    % blockerExp = blockerExpAlt
    blockerExp=1;
    O2bath = M('O2bath')*ones(size(t));
    O2bath(t>M('tstart') & t<M('tend'))=M('perc')*M('O2bath');

    % Na-K pump
    blockerExp=1;
    rho = 1./(1+exp((20-O2)/3));
    sigmapump = 1./7.*(exp(NaCe./67.3)-1);
    fpump = 1./(1+0.1245.*exp(-0.1.*M('F')./M('R')./M('T').*V) + ...
               0.0365.*sigmapump.*exp(-M('F')./M('R')./M('T').*V));
    Ipumpi = rho*blockerExp.*M('pumpScaleNeuron').*fpump.*M('PNKAi').*( ...
                NaCi.^(1.5)./(NaCi.^(1.5)+M('nka_na').^1.5)).*(KCe./(KCe+M('nka_k')));

    % KCl cotransport
    JKCl = M('UKCl').*M('R').*M('T')./M('F').*(log(KCi)+log(ClCi)-log(KCe)-log(ClCe));

    % NCX
    INCXi = M('PNCXi').*(NaCe.^3)./(M('alphaNaNCX').^3+NaCe.^3).*(...
       CaCc./(M('alphaCaNCX')+CaCc)).*( ...
          NaCi.^3./NaCe.^3.*exp(M('eNCX').*M('F').*V./M('R')./M('T')) - ...
          CaCi./CaCc.*exp((M('eNCX')-1).*M('F').*V./M('R')./M('T')))./( ...
             1+M('ksatNCX').*exp((M('eNCX')-1).*M('F').*V./M('R')./M('T')));

    % EAAT
    beta_EAAT=0.0292; %mV^-1 from Flanagan PLOS CB 2018
    V_EAATi = M('R')*M('T')/M('F')/2*log(NaCe.^3./NaCi.^3.*KCi./KCe .* ...
                                          M('HeOHa').*GluCc./GluCi);
    % JEAATi = M('PEAATi')*exp(-beta_EAAT*(Vi-V_EAATi));
    JEAATi = M('PEAATi')*(exp(-beta_EAAT*(Vi-V_EAATi))-1); % Version Breslin 2018
    % JEAATi =  M('PEAATi').*M('R').*M('T')./M('F').*log((NaCe.^3)./(NaCi.^3) .* ...
    %                                 KCi./KCe.*M('HeOHai').*GluCc./GluCi);
    % =========================================================================
    % ---------------------------------------------------------------------
    % ========================================================================

    k1 = M('k1max').*CaCi./(CaCi+M('KM'));
    gCa = CaCi./(CaCi+M('KDV'));
    k2 = M('k20')+gCa.*M('k2cat');
    kmin2cat = M('k2cat').*M('kmin20')./M('k20');
    kmin2 = M('kmin20')+gCa.*kmin2cat;
    docking = 1./(1+exp(100*(ND-2.5))); %Saturation of docking glutamate.
    % ===========================================================================
    % --------------------------ASTROCYTE-----------------------------------------
    % ============================================================================

    % Na-K pump -- Oxygen, see above
    sigmapumpA = 1./7.*(exp(NaCe./67.3)-1);
    fpumpA = 1./(1+0.1245.*exp(-0.1.*M('F')./M('R')./M('T').*Vg) + ...
                0.0365.*sigmapumpA.*exp(-M('F')./M('R')./M('T').*Vg));
    Ipumpg = rho*M('pumpScaleAst').*blockerExp.*fpumpA.*M('PNKAg').*( ...
          NaCg.^(1.5)./(NaCg.^(1.5)+M('nka_na_g').^1.5)).*(KCe./(KCe+M('nka_k_g')));

    % Leak
    IKLg = M('PKLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((KCg - ...
                     KCe.*exp((-M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((-M('F').*Vg)./(M('R').*M('T')))));
    IClLg = M('PClLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((ClCg - ...
                     ClCe.*exp((M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((M('F').*Vg)./(M('R').*M('T')))));
    INaLg = M('PNaLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((NaCg - ...
                     NaCe.*exp((-M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((-M('F').*Vg)./(M('R').*M('T')))));
    IGluLg = M('PGluLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((GluCg - ...
                     GluCc.*exp((M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((M('F').*Vg)./(M('R').*M('T')))));
    ICaLg = 4.*M('PCaLg').*M('F').^2./( ...
       M('R').*M('T')).*Vg.*((CaCg - ...
                     CaCc.*exp((-2.*M('F').*Vg)./(M('R').*M('T'))))./( ...
                        1-exp((-2.*M('F').*Vg)./(M('R').*M('T')))));

    % NKCC1
    JNKCC1 = M('PNKCC1').*M('R').*M('T')./M('F').*(log(KCe) + log(NaCe) ...
                                   + 2.*log(ClCe) - log(KCg) ...
                                   - log(NaCg) - 2.*log(ClCg));
    % Kir4.1
    Vkg = M('R').*M('T')./M('F').*log(KCe./KCg);
    minfty = 1./(2+exp(1.62.*(M('F')./M('R')./M('T')).*(Vg-Vkg)));
    IKir = M('PKir').*minfty.*KCe./(KCe+M('KCe_thres')).*(Vg-Vkg);
    % IKir = M('GKir.*(Vg-Vkg).*sqrt(KCe)./(1+exp((Vg - Vkg - 34)./19.23)));

    % GLT-1
    beta_EAAT=0.0292; %mV^-1 from Flanagan PLOS CB 2018
    V_EAATg = M('R')*M('T')/M('F')/2*log(NaCe.^3./NaCg.^3.*KCg./KCe .* ...
                                          M('HeOHa').*GluCc./GluCg);
    % JEAATg = M('PEAATg')*exp(-beta_EAAT*(Vg-V_EAATg));
    JEAATg = M('PEAATg')*(exp(-beta_EAAT*(Vg-V_EAATg))-1); %Version Breslin 2018
%OLD; reversal potential as current...
    % JEAATg = M('PEAATg').*M('R').*M('T')./M('F').*log(NaCe.^3./NaCg.^3.*KCg./KCe .* ...
    %                                       M('HeOHa').*GluCc./GluCg);

    % NCX
    INCXg = M('PNCXg').*(NaCe.^3)./(M('alphaNaNCX').^3 + ...
                               NaCe.^3).*(CaCc./(M('alphaCaNCX')+CaCc)).*( ...
                                  NaCg.^3./NaCe.^3.*exp(M('eNCX').*M('F').*Vg./M('R')./M('T')) - ...
                                  CaCg./CaCc.*exp((M('eNCX')-1).*M('F').*Vg./M('R')./M('T')))./(...
                                     1+M('ksatNCX').*exp( ...
                                        (M('eNCX')-1).*M('F').*Vg./M('R')./M('T')));

    % =========================================================================
    % ----------------------------VOLUME DYNAMICS------------------------------
    % =========================================================================
    SCi = NaCi+KCi+ClCi+M('NAi')./Wi ;
    SCe = NaCe+KCe+ClCe+M('NAe')./We + M('NBe')./We;
    SCg = NaCg + KCg + ClCg + M('NAg')./Wg + M('NBg')./Wg;
    delpii = M('R').*M('T').*(SCi-SCe);
    fluxi = M('LH20i').*(delpii);
    delpig = M('R').*M('T').*(SCg-SCe);
    fluxg = M('LH20g').*(delpig);

    Voli = Wi./M('Wi0').*100;
    Volg = Wg./M('Wg0').*100;

    % =========================================================================
    % ----------------------------POSTSYNAPTIC RESPONSE------------------------
    % =========================================================================

   % IAMPA = M('gAMPA.*mAMPA.*(Vpost-M('VAMPA)

    % ==========================================================================
    % ----------------------------INTERVENTIONS---------------------------------
    % ==========================================================================

        if M('block') ==1
            block=block{1};
        for i = 1:size(block,1) %Each row has a seperate label that needs to be blocked.
            key = block(i,1);
            val_start = str2double(block(i,2));
            val_end = str2double(block(i,3));
            minpercentage =str2double(block(i,4));
            blockOther = (1./(1+exp(M('block_graduallity').*(t-val_start))) + ...
                          1./(1+exp(-M('block_graduallity2').*(t-val_end))));        %FIM: AANGEPAST zodat ik de block abrubter kan maken. 
            blockOther = minpercentage+(1-minpercentage)*blockOther;              %FIM: aangepast zodat de block abrubter naar beneden kan
            plot(t,blockOther)
            %fprintf('blocking %s current from time %4.2f untill time %4.2f \n', key,val_start,val_end)
            if key == 'INaG'
                INaG = INaG.*blockOther;
            elseif key == 'IKG'
                IKG = IKG.*blockOther;
            elseif key == 'IClG'
                IClG = IClG.*blockOther;
            elseif key == 'JKCl'
                JKCl = JKCl.*blockOther;
            elseif key == 'ICaG'
                ICaG = ICaG.*blockOther;
            elseif key == 'INCXi'
                INCXi = INCXi.*blockOther;
            elseif key == 'JEAATi'
                JEAATi = JEAATi.*blockOther;
            elseif key == 'IKir'
                IKir = IKir.*blockOther;
            elseif key == 'JNKCC1'
                JNKCC1 = JNKCC1.*blockOther;
            elseif key == 'JEAATg'
                JEAATg = JEAATg.*blockOther;
            elseif key == 'INCXg'
                INCXg = INCXg.*blockOther;
            elseif key == 'WaterN'
                fluxi = fluxi.*blockOther;
            elseif key == 'WaterA'
                fluxg = fluxg.*blockOther;
            elseif key == 'IClLg'
                partialLeakblock = val_(2) + (1-val_(2)).*blockOther;
                IClLg = IClLg.*partialLeakblock;
            end
        end
    elseif M('block') == 0  %This means that block=0 and so nothing happens
    else                            %If block is not 0 and not a string of length 3 (block = ["BLOCK1", starttime, endtime]), something is wrong.
        disp('block is not well defined. Make sure to define it as [["BLOCK1", starttime, endtime]; ["BLOCK2", starttime, endtime], ...]')
    end

    if M('excite')== 1
%HGE 3-5-24: Added tt to have a phase difference, i.e. stimulation at odd minutes, and OGD onset + offset at even minutes 
        tt=t+1;
        blocker_Excite = 1 - (1./(1+exp(1000.*(tt-M('excite_1')))) + ...
                              1./(1+exp(-1000.*(tt-M('excite_2')))));
        %IExcite = blocker_Excite.*arg_excite(2)./2./M('F').*(1-square(array(5.*t),duty=arg_excite(3)));
        dur_ = M('excite_4');
        duty_ = M('excite_5');
%HGE 17-1-24:Changed to make the pulse shorter, a little bit stronger, just a few spikes, and simpler notation.
        IExcite = M('excite_3')/M('F')*blocker_Excite.*(1+square(2*pi*tt/dur_,100*duty_))/2;
        % IExcite = blocker_Excite.*M('excite_3')./2./M('F').*(1-square(2.*pi.*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));%Original
    elseif M('excite') == 0
        IExcite = 0    ;
        %IExcite = blocker_Excite.*4.5./M('F')
    end
    if M('excite2')== 1
        blocker_Excite2 = 1 - (1./(1+exp(100.*(t-M('excite2_1')))) + ...
                              1./(1+exp(-100.*(t-M('excite2_2')))));
        %IExcite2 = blocker_Excite2.*arg_excite2(2)./2./M('F').*(1-signal.square(array(5.*t),duty=arg_excite2(3)));
        dur_ = M('excite2_4');
        duty_ = M('excite2_5');
        IExcite2 = blocker_Excite2.*M('excite2_3')./2./M('F').*(1-square(2.*pi.*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));
    elseif M('excite2')==0
        IExcite2=0;
    end

    if M('excite3')== 1
        blocker_Excite3 = 1 - (1./(1+exp(100.*(t-M('excite3_1')))) + ...
                              1./(1+exp(-100.*(t-M('excite3_2')))));
        %IExcite2 = blocker_Excite2.*arg_excite2(2)./2./M('F').*(1-signal.square(array(5.*t),duty=arg_excite2(3)));
        dur_ = M('excite3_4');
        duty_ = M('excite3_5');
        IExcite3 = blocker_Excite3.*M('excite3_3')./2./M('F').*(1-square(2.*pi.*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));
    elseif M('excite3')==0
        IExcite3=0;
    end

    if M('exciteK')== 1
        blocker_Excite = 1 - (1./(1+exp(1000.*(t-M('exciteK_1')))) + ...
                              1./(1+exp(-1000.*(t-M('exciteK_2')))));
        %IExcite = blocker_Excite.*arg_excite(2)./2./M('F').*(1-square(array(5.*t),duty=arg_excite(3)));
        dur_ = M('exciteK_4');
        duty_ = M('exciteK_5');
        IExciteK = blocker_Excite.*M('exciteK_3')./2./M('F').*(1-square(2*pi*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));
    elseif M('exciteK') == 0
        IExciteK = 0    ;
        %IExcite = blocker_Excite.*4.5./M('F')

    end

    if M('exciteK2')== 1
        blocker_Excite = 1 - (1./(1+exp(1000.*(t-M('exciteK2_1')))) + ...
                              1./(1+exp(-1000.*(t-M('exciteK2_2')))));
        %IExcite = blocker_Excite.*arg_excite(2)./2./M('F').*(1-square(array(5.*t),duty=arg_excite(3)));
        dur_ = M('exciteK2_4');
        duty_ = M('exciteK2_5');
        IExciteK2 = blocker_Excite.*M('exciteK2_3')./2./M('F').*(1-square(2*pi*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));
    elseif M('exciteK2') == 0
        IExciteK2 = 0    ;
        %IExcite = blocker_Excite.*4.5./M('F')
    end

    if M('InjectCaNeuron')==1
      blocker_Excite = 1 - (1./(1+exp(1000.*(t-M('InjectCaNeuron_1')))) + ...
                              1./(1+exp(-1000.*(t-M('InjectCaNeuron_2')))));
        %IExcite = blocker_Excite.*arg_excite(2)./2./M('F').*(1-square(array(5.*t),duty=arg_excite(3)));
        dur_ = M('InjectCaNeuron_4');
        duty_ = M('InjectCaNeuron_5');
        IInjectCaNeuron = blocker_Excite.*M('InjectCaNeuron_3')./2./M('F').*(1-square(2*pi*(t+2.1).*(1-duty_)./(dur_./60),100*duty_));
    elseif M('InjectCaNeuron') == 0
        IInjectCaNeuron = 0    ;
        %IExcite = blocker_Excite.*4.5./M('F')
    end

    if M('InjectCaAst')==1
        tt=t+1;
        blocker_Excite = 1 - (1./(1+exp(1000.*(tt-M('InjectCaAst_1')))) + ...
                              1./(1+exp(-1000.*(tt-M('InjectCaAst_2')))));
        %IExcite = blocker_Excite.*arg_excite(2)./2./M('F').*(1-square(array(5.*t),duty=arg_excite(3)));
        dur_ = M('InjectCaAst_4');
        duty_ = M('InjectCaAst_5');
        IInjectCaAst = blocker_Excite.*M('InjectCaAst_3')./2./M('F').*(1-square(2*pi*(tt+2.1).*(1-duty_)./(dur_./60),100*duty_));
    elseif M('InjectCaAst') == 0
        IInjectCaAst = 0    ;
        %IExcite = blocker_Excite.*4.5./M('F')
    end


    if M('astblock')==1
        startastblock = M('astblock_1');
        endastblock = M('astblock_2');
        astblock = (1./(1+exp(500.*(t-startastblock))) + ...
            1./(1+exp(-500.*(t-endastblock))));
    elseif M('astblock') ==0
        astblock = 1;
    else
        error('Astblock is not a vector of [TSART, TEND] and not 0. Make sure that you assign the right value to Astblock')
    end

    % ==========================================================================
    % ----------------------------FINAL MODEL-----------------------------------
    % ==========================================================================

    ODEs = [ ... % Neuron
       ((-1./M('F').*(INaG+INaLi+3.*Ipumpi))-synapse_block.*3./M('F').*INCXi + ...
        synapse_block.*3.*JEAATi ), ...
       (-1./M('F').*(IKG+IKLi-2.*Ipumpi)-synapse_block.*JEAATi - JKCl), ...
       (1./M('F').*(IClG+IClLi)) - JKCl, ...
       gates_block.*(alpham.*(1-m)-betam.*m), ...
       gates_block.*(alphah.*(1-h)-betah.*h), ...
       gates_block.*(alphan.*(1-n)-betan.*n), ...
       synapse_block.*(-1./2./M('F')).*(ICaG+ICaLi -INCXi), ...
       ... % GLUTAMATE RECYCLING
       (k1.*ND-(M('kmin1')+k2).*NN+kmin2.*NR), ...
       (k2.*NN-(kmin2+3.*M('k3').*CaCi).*NR + M('kmin3').*NR1), ...
       (3.*M('k3').*CaCi.*NR-(M('kmin3')+2.*M('k3').*CaCi).*NR1+2.*M('kmin3').*NR2), ...
       (2.*M('k3').*CaCi.*NR1-(2.*M('kmin3')+M('k3').*CaCi).*NR2+3.*M('kmin3').*NR3), ...
       (M('k3').*CaCi.*NR2-(3.*M('kmin3')+M('k4')).*NR3), ...
       synapse_block.*(-docking.*NI./M('trec') + JEAATi + 1./M('F').*IGluLi), ...
       (docking.*NI./M('trec')-k1.*ND+M('kmin1').*NN), ...
       ... % ASTROCYTE
       astblock.*((-1./M('F')).*(3.*Ipumpg + INaLg + ...
                           synapse_block.*3.*INCXg )+JNKCC1+synapse_block.*3.*JEAATg), ...
       astblock.*((-1./M('F')).*(-IKir - 2.*Ipumpg + IKLg)+ ...
                 + JNKCC1-synapse_block.*JEAATg), ...
       astblock.*(2.*JNKCC1 + 1./M('F').*IClLg) , ...
       synapse_block.*astblock.*(-1./2./M('F')).*(-INCXg + ICaLg), ...
       ... % POSTSYN
       synapse_block.*astblock.*(JEAATg + 1./M('F').*IGluLg), ... % 1./(M('tpost).*(-(Vpost-M('Vpost0)-M('Rm.*IAMPA),\
       ... % WATER
       fluxi, ...
       astblock.*fluxg];

    if M('excite')== 1
        %ODEs(19) =  M('F')./M('C').*(ODEs(0)+ODEs(1)+synapse_block.*2.*(ODEs(6))-synapse_block.*(ODEs(7)+ODEs(8)+ODEs(9)+ODEs(10)+ODEs(11)+ODEs(12)+ODEs(13))-ODEs(2) + IExcite+IExcite2)
        ODEs(:,1) = ODEs(:,1) + IExcite + IExcite2 + IExcite3; % Inject current into the neuron in the form of a bit of sodium injection
    end
    if M('exciteK')== 1
        %ODEs(19) =  M('F')./M('C').*(ODEs(0)+ODEs(1)+synapse_block.*2.*(ODEs(6))-synapse_block.*(ODEs(7)+ODEs(8)+ODEs(9)+ODEs(10)+ODEs(11)+ODEs(12)+ODEs(13))-ODEs(2) + IExcite+IExcite2)
            ODEs(:,16) = ODEs(:,16) + IExciteK-IExciteK2; % Inject K ions into the astrocyte 
    end

    if M('InjectCaNeuron')==1
         ODEs(:,7) = ODEs(:,7) + IInjectCaNeuron; % Inject Ca ions into the neuron
    end
    if M('InjectCaAst')==1
         ODEs(:,18) = ODEs(:,18) + IInjectCaAst; % Inject Ca ions into the neuron
    end


    ODEs = ODEs.*60.*1e3;
    ODEs=ODEs';

    %% The Plots

colororder({'[0 0.5 0]'})
ax.LineStyleOrder = {'-','-'};
tiledlayout(2,2, 'Padding', 'none', 'TileSpacing', 'compact');

if M('excite')
    hold on
    subplot(6,2,1)
    plot(t,M('F')*(IExcite+IExcite2+IExcite3),'k',LineWidth=2)
    patch([M('tstart') M('tend') M('tend') M('tstart')],max(M('F')*IExcite)*[0 0 1 1],[.2 .2 .2],'FaceAlpha',.2);
    title('Applied current')

    subplot(6,2,2)
    hold on
    plot(t,M('F')*(IExcite+IExcite2+IExcite3),'k',LineWidth=2)
    patch([M('tstart') M('tend') M('tend') M('tstart')],max(M('F')*IExcite)*[0 0 1 1],[.2 .2 .2],'FaceAlpha',.2);
    title('Applied current')
end

subplot(6,2,3)
hold on
plot(t,NaCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) %Astrocyte
plot(t,NaCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,NaCe, 'color', '[0 0.5 0]', LineWidth=2)          %ECS
title('[Na^+] (mM)')
yyaxis left
%xlabel('t')

subplot(6,2,4)
hold on
plot(t,KCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
plot(t,KCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,KCe, 'color', '[0 0.5 0]', LineWidth=2)
title('[K^+] (mM)')
yyaxis left
%xlabel('t')

subplot(6,2,5)
hold on
plot(t,ClCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
plot(t,ClCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,ClCe, 'color', '[0 0.5 0]', LineWidth=2)
title('[Cl^-] (mM)')
yyaxis left
%xlabel('t')

subplot(6,2,6)
hold on
plot(t,GluCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
plot(t,GluCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,GluCc, 'color', '[0 0.5 0]', LineWidth=2)
title('[Glu] (mM)')
yyaxis left
%xlabel('t')

subplot(6,2,7)
hold on
plot(t,CaCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
plot(t,CaCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,CaCc, 'color', '[0 0.5 0]', LineWidth=2)
title('[Ca^{2+}] (mM)')
yyaxis left
%xlabel('t')

colororder({'k','[0.9290 0.6940 0.1250]'})
subplot(6,2,8)
hold on

plot(t,Voli, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
plot(t,Volg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
title('Volume increase (%)')
%xlabel('t')

subplot(6,2,9)
hold on

plot(t,Vi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
plot(t,Vg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
title('Mem. Potential (mV)')
%xlabel('t')

subplot(6,2,10)
hold on
plot(t,M('F')*JEAATi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,M('F')*JEAATg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
title('Forward EAAT current (pA)')
yyaxis left
%xlabel('t')

subplot(6,2,11)
hold on
plot(t,-INCXi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
yyaxis right
plot(t,-INCXg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
title('Forward NCX current (pA)')
yyaxis left
xlabel('t (min)')

subplot(6,2,12)
hold on
plot(t,Ipumpi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
plot(t,Ipumpg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2)
title('NKA current (pA)')
xlabel('t (min)')

if ~M('perc')==1
  if ~ M('excite')
    subplot(6,2,1)
    hold on
    plot(t,blockerExp,'k',LineWidth=2)
    title('Available energy')

    subplot(6,2,2)
    hold on
    plot(t,blockerExp,'k',LineWidth=2)
    title('Available energy')
  end

  h = zeros(3, 1);
  h(1) = plot(NaN,NaN,'color', '[0 0.4471 0.7412]', LineWidth=2); %neuron;
  h(2) = plot(NaN,NaN,'color', '[0.9290 0.6940 0.1250]', LineWidth=2);  %Astrocyte;
  h(3) = plot(NaN,NaN,'color', '[0 0.5 0]', LineWidth=2); %ECS or cleft;
  lgd=legend(h, 'Neuron','Astrocyte','ECS');
  lgd.Position=[0.485,0.85,0.06,0.06];

  for i=1:12
    subplot(6,2,i)
    hold on
    ylimits=ylim();
    ylim(ylimits)
    xlim([M('t0'),M("tfinal")])
    rectangle('Position', [M('tstart'),-300,(M('tend')-M('tstart')),600], 'FaceColor',[.7 .7 .7], 'EdgeColor', [.7 .7 .7])
    if M('block') ==1
      for j = 1:size(block,1)
        rectangle('Position', [str2double(block(j,2)),-300,(str2double(block(j,3))-str2double(block(j,2))),600], 'FaceColor',[.75 .86 .63], 'EdgeColor', [.75 .86 .63]);
      end
    end
    ax=gca;
    ax.Children = ax.Children(length(ax.Children):-1:1);
  end
  h(4) = plot(NaN,NaN, 'color',[.7 .7 .7] , LineWidth=10);
  lgd=legend(h, 'Neuron','Astrocyte','ECS','Energy deprivation');
  lgd.Position=[0.485,0.85,0.06,0.06];
end

%Legend:

savefig(figure(1),[pwd '/images/' filename '/plotall'])
set(gcf, 'Position', get(0, 'Screensize'));
saveas(figure(1),[pwd '/images/' filename '/plotall.png'])

% figure(2)
% plot(t,IInjectCaNeuron)

% figure(3)
% plot(t,NGluc+NGlug+NGlui+ND+NN+NR+NR1+NR2+NR3)
% hold on
% plot(t,NGluc+NGlug+NGlui)
% figure(4)
% plot(ND+NN+NR+NR1+NR2+NR3)



%% plots tweede grafiek (alleen handmatig voor nu)
% return
% 
% if M('excite')
%     hold on
%     subplot(6,2,1)
%     plot(t,M('F')*(IExcite+IExcite2),'k',LineWidth=2)
%     title('Applied current')
% 
%     subplot(6,2,2)
%     hold on
%     plot(t,M('F')*(IExcite+IExcite2),'k',LineWidth=2)
%     title('Applied current')
% end
% 
% M('perc')
% if M('perc')
%     subplot(6,2,1)
%     hold on
%     yyaxis right
%     plot(t,blockerexp,'k',LineWidth=2)
%     title('Applied current')
% 
%     subplot(6,2,2)
%     hold on
%     plot(t,blockerexp,'k',LineWidth=2)
%     title('Applied current')
% end
% 
% 
% subplot(6,2,3)
% hold on
% yyaxis left
% plot(t,NaCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) %Astrocyte
% plot(t,NaCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% hold on
% plot(t,NaCe, 'color', '[0 0.5 0]', LineWidth=2)          %ECS
% title('Na (Mm)')
% %xlabel('t')
% 
% subplot(6,2,4)
% hold on
% yyaxis left
% plot(t,KCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% plot(t,KCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% plot(t,KCe, 'color', '[0 0.5 0]', LineWidth=2)         
% title('K (Mm)')
% %xlabel('t')
% 
% subplot(6,2,5)
% hold on
% yyaxis left
% plot(t,ClCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% plot(t,ClCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% plot(t,ClCe, 'color', '[0 0.5 0]', LineWidth=2)         
% title('Cl (Mm)')
% %xlabel('t')
% 
% subplot(6,2,6)
% hold on
% yyaxis left
% plot(t,GluCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% plot(t,GluCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% plot(t,GluCc, 'color', '[0 0.5 0]', LineWidth=2)         
% title('Glu (Mm)')
% %xlabel('t')
% 
% subplot(6,2,7)
% hold on
% yyaxis left
% plot(t,CaCg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% plot(t,CaCi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% plot(t,CaCc, 'color', '[0 0.5 0]', LineWidth=2)         
% title('Ca (Mm)')
% %xlabel('t')
% 
% %colororder({'k','[0.9290 0.6940 0.1250]'})
% 
% subplot(6,2,8)
% hold on
% plot(t,Voli, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% plot(t,Volg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% title('Volume increase (%)')
% %xlabel('t')
% 
% subplot(6,2,9)
% hold on
% plot(t,Vi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %neuron
% plot(t,Vg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% title('Mem. Potential (mV)')
% %xlabel('t')
% 
% subplot(6,2,10)
% hold on
% yyaxis left
% plot(t,M('F')*JEAATi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% plot(t,M('F')*JEAATg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% title('Forward EAAT current (pA)')
% %xlabel('t')
% 
% subplot(6,2,11)
% hold on
% yyaxis left
% plot(t,-INCXi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% yyaxis right
% plot(t,-INCXg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% title('Forward NCX current (pA)')
% %xlabel('t')
% 
% subplot(6,2,12)
% hold on
% plot(t,Ipumpi, 'color', '[0 0.4471 0.7412]', LineWidth=2) %Neuron
% plot(t,Ipumpg, 'color', '[0.9290 0.6940 0.1250]', LineWidth=2) 
% title('NKA current (pA)')
% 
% h(4) = plot(NaN,NaN,'-k', LineWidth=2) %neuron;
% h(5) = plot(NaN,NaN,'--k', LineWidth=2)  %Astrocyte;
